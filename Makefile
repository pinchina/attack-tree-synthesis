BUILDCMD=ocamlbuild -use-ocamlfind -r -classic-display -cflag -dtypes -cflag -bin-annot -cflag -w -cflag +a-27

# TEST is to be defined for testing
PPTEST=-pp "camlp4o pa_macro.cmo -DTEST"
PPNOTEST=-pp "camlp4o pa_macro.cmo -UTEST"


all: 
	$(BUILDCMD) $(PPTEST) synthese.byte
	cp -f synthese.byte synthese
	$(BUILDCMD) $(PPTEST) tests.byte
	cp -f tests.byte tests
	$(BUILDCMD) $(PPTEST) test_generation.byte
	cp -f test_generation.byte test_generation
	$(BUILDCMD) $(PPTEST) meta_test_generation.byte
	cp -f meta_test_generation.byte meta_test_generation
	$(BUILDCMD) $(PPTEST) generer_exemples_arbres.byte
	cp -f generer_exemples_arbres.byte generer_exemples_arbres
	$(BUILDCMD) $(PPTEST) run_time_testing.byte
	cp -f run_time_testing.byte run_time_testing

notest: 
	$(BUILDCMD) $(PPNOTEST) synthese.byte
	cp -f synthese.byte synthese

binary: 
	$(BUILDCMD) $(PPNOTEST) synthese.native
	cp -f synthese.native synthese

clean:
	$(BUILDCMD) -clean
	rm -f synthese

