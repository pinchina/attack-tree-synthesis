(******************* META_TEST_GENERATION ************************)

(** Using module Test_generation to generate numerous instances. **)

open Test_generation


print_endline "Switch to manual mode? (y for yes)";

let manual = (compare (read_line ()) "y" = 0) in

if not manual then
	begin
		print_endline "Min trace length (>= 2):";
		min_trace_length := int_of_string (read_line ()) - 1;
		print_endline "Max trace length (>= 2):";
		max_trace_length := int_of_string (read_line ()) - 1;

		print_endline "Min number of children for AND:";
		min_AND_arity := int_of_string (read_line ());
		print_endline "Max number of children for AND:";
		max_AND_arity := int_of_string (read_line ());

		print_endline "Min maximal number of leaves per OR:";
		min_OR_arity_bound := int_of_string (read_line ());
		print_endline "Max maximal number of leaves per OR:";
		max_OR_arity_bound := int_of_string (read_line ());

		print_endline "How many tests per parameters?";
		number_of_tests := int_of_string (read_line ());

	end;

	generate_tests manual
;;
