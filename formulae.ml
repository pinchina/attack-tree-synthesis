(******************* FORMULAE ************************)

open Types
open Exceptions
open Utilities

(***

	/!\ À FAIRE OU PAS ? 

	Si les règles de raffinement en entrée ne sont pas supposées nécessairement aplaties,
	alors écrire une fonction de normalisation pour les transformer ainsi.

***)



(* /!\ Dispensable (car simple) ? Ou utile pour un "fold" ? *)
(* ancillary function: adding a variable through disjunction *)
let append_disjunct (_formula_1: formula) (_formula_2: formula): formula =
	Or (_formula_1, _formula_2)
;;



(** /!\ SI OPTIMISATION AVEC "fold", DEVIENDRA INUTILE **)
(** /!\ SI SAND BINAIRES, inutile ?? À vérifier.
  /!\ Sert aussi dans "generic_single_conjunction" actuellement. **)
(* utility ancillary function: generate the list [f 0 ; ... ; f (n-1)] *)
(* tail-recursive version; reference: https://stackoverflow.com/questions/42110714/cores-list-init-in-pervasives *)
let init_list (n: int) ~f =
  let rec init_list' acc i n f =
    if i >= n then acc
    else init_list' ((f i) :: acc) (i+1) n f
  in List.rev (init_list' [] 0 n f)
;;



(** /!\ SI OPTIMISATION AVEC "fold", DEVIENDRA INUTILE **)
(* ancillary function: turns a list of formulae into the disjunction of these formulae *)
(* PRECONDITION: the formulae list is not empty *)
let rec list_to_disjunction (formula_list: formula list): formula =
	match formula_list with
	| [] -> raise (Empty_list (standard_error_message "list_to_disjunction" empty_list_error_message)) (* NB: we could use "up tack"/"falsum" ("false") as a default, mais ce cas n'est pas censé survenir et alourdirait les disjonctions d'un terme *)
	| [_formula] -> _formula
	| _formula::remaining_formulae -> Or (_formula, list_to_disjunction remaining_formulae)
;;


(* find all refinement rules associated to a non-terminal goal in the library *)
let rules_of_goal (_library: library) (_goal: nonterminal_goal): refinement_rule list =
	try
		let refinements = List.assoc _goal _library in
		List.map (fun _refinement -> (_goal, _refinement)) refinements
	with
		Not_found -> raise (No_goal_exception (standard_error_message "rules_of_goal" no_goal_error_message))
;;


(* generate the instantiation formula for the goals with the corresponding rules *)
let rec instantiate (_formula: formula) (_library: library): formula = match _formula with
	| True -> True
	| False -> False
	| Atom _variable ->
		begin
		match _variable with
			| REFINEMENT_VAR _ -> _formula
			| INTERVAL_VAR _ -> _formula

			(* create the disjunction of all x^R(g)_{i, j} *)
			| GOAL_VAR (_goal, _start, _end) ->
				let variables_of_rule (_rule: refinement_rule) = Atom (REFINEMENT_VAR (_rule, _start, _end)) in
				try
					let variables_of_goal = List.map variables_of_rule (rules_of_goal _library _goal) in
					list_to_disjunction variables_of_goal
				with
				| No_goal_exception error_message -> failwith (standard_error_message "instantiate" error_message)

		end
	| Not _formula_1 -> Not (instantiate _formula_1 _library)
	| And (_formula_1, _formula_2) -> And (instantiate _formula_1 _library, instantiate _formula_2 _library)
	| Or (_formula_1, _formula_2) -> Or (instantiate _formula_1 _library, instantiate _formula_2 _library)
	| Implies (_formula_1, _formula_2) -> Implies (instantiate _formula_1 _library, instantiate _formula_2 _library)
;;



(**-------------------- INTERVAL VARIABLES -----------------------**)
(* ancillary function to process AND *)


(* ancillary function: turns a list of formulae into the conjunction of these formulae *)
(* PRECONDITION: the formulae list is not empty *)
let rec list_to_conjunction (formula_list: formula list): formula =
	match formula_list with
	| [] -> raise (Empty_list (standard_error_message "list_to_conjunction" empty_list_error_message)) (* N.B. : on pourrait mettre "truc" ("vrai") par défaut, mais ce cas n'est pas censé survenir et alourdirait les conjonctions d'un terme *)
	| [_formula] -> _formula
	| _formula::remaining_formulae -> And (_formula, list_to_conjunction remaining_formulae)
;;


(* /!\ Optimisable ? En passant par un "fold_left" directement plutôt que "init_list" puis "list_to_conjunction" ? *)
(* generate the iterated operation of a formula parameterised by a single index ranging from start_index to end_index *)
(* "list_to_operation" turns a list of formulae into the iterated application of the operation
	on formulae
*)
let generic_single_operation (list_to_operation: formula list -> formula) (start_index: int) (end_index: int) (parameterized_formula: int -> formula): formula =
	let formulae_list = init_list (end_index - start_index + 1) (fun index -> parameterized_formula (start_index + index)) in
	list_to_operation formulae_list
;;


(* generate the conjunction of a formula parameterised by a single index ranging from start_index to end_index *)
let generic_single_conjunction: (int -> int -> (int -> formula) -> formula) =
	generic_single_operation list_to_conjunction
;;


(* generate the disjunction of a formula parameterised by a single index ranging from start_index to end_index *)
let generic_single_disjunction: (int -> int -> (int -> formula) -> formula) =
	generic_single_operation list_to_disjunction
;;


(** Big conjunction function: taking a function as a parameter (= parameterised formula)
	and generating for i ranging from start_index to end_index and j ranging from i to end_index;
	the optional parameter "forbidden" enables pinpointing a forbidden ordered pair of values
 **)
(* NB: "real_start" is a technical detail used to manage the case where the forbidden value would be the starting
   ordered pair, in which case we need to change the initialisation of "temp_formula";
   otherwise, we could make use of initialising to "True", but this would vainly lengthen the formulae.
*)
(* /!\ À VÉRIFIER : problème si start_index = end_index ?
  Mais jamais appelé ainsi ? À préciser en précondition, si besoin. *)
let generic_double_conjunction ?(forbidden: int * int = (-1, -1)) (start_index: int) (end_index: int) (parameterized_formula: int -> int -> formula): formula = 
	let real_start = 
		if (start_index, start_index) = forbidden
		then start_index + 1
		else start_index
	in
	let temp_formula = ref (parameterized_formula start_index real_start)
	in
	for i = start_index to end_index do
		for j = i to end_index do
			if not ((i, j) = (start_index, real_start))
			&& not ((i, j) = forbidden)
			then
				temp_formula := And(!temp_formula, parameterized_formula i j)
		done
	done;
	!temp_formula
;;


(* same as "generic_double_conjunction", but for a disjunction,
	and without forbidden values
*)
(* only used in "unique_formula" *)
let generic_double_disjunction (start_index: int) (end_index: int) (parameterized_formula: int -> int -> formula): formula = 
	let temp_formula = ref (parameterized_formula start_index start_index)
	in
	for i = start_index to end_index do
		for j = i to end_index do
			if not ((i, j) = (start_index, start_index))
			then
				temp_formula := Or(!temp_formula, parameterized_formula i j)
		done
	done;
	!temp_formula
;;

(* /!\ À VÉRIFIER : problème si start_index = end_index ? Seulement selon "forbidden" ?
  Mais jamais appelé ainsi ? À préciser en précondition, si besoin.
  -> RÉGLÉ via traitement dès le début. *)
(* creating the unicity formula: "unique_formula R k i j"
	NB: written as the conjunction of an existential part (existence_formula)
	and a unicity constraint part (implications).
		This is equivalent to a big XOR. Notice the existential part is useful
		in the synthesis, since every subgoal of an AND-rule has to be instantiated.
*)
let unique_formula (rule: refinement_rule) (which_subrule: int) (start_index: int) (end_index: int): formula =
	let existence_formula =
		generic_double_disjunction
			start_index
			end_index
			(fun i j -> Atom (INTERVAL_VAR (rule, start_index, end_index, which_subrule, i, j)))
	in
	if start_index = end_index
	then existence_formula
	else
		And(
			existence_formula,

			generic_double_conjunction
			start_index
			end_index
			(
				fun i1 j1 ->
				Implies (
					Atom (INTERVAL_VAR (rule, start_index, end_index, which_subrule, i1, j1))
					,
					generic_double_conjunction
					start_index
					end_index
					(fun i2 j2 -> Not (Atom (INTERVAL_VAR (rule, start_index, end_index, which_subrule, i2, j2))))
					~forbidden: (i1, j1)
				)
			)
		)
;;


(* internal formula relative to the first big conjunction
	from the second "block" of formula psi for the AND *)
(* NB: "which_subrule" has to lie between 1 (and not 0) and the refinement rule arity *)
let sound_interval_variable (rule: refinement_rule) (which_subrule: int) (start_index: int) (end_index: int) (_library: library): formula =
	let subgoals = snd (snd rule) in
	generic_double_conjunction
	start_index
	end_index
	(
		fun i1 j1 ->
		Implies (
			Atom (INTERVAL_VAR (rule, start_index, end_index, which_subrule, i1, j1))
			,
			try
				let NONTERMINAL_GOAL which_subgoal = List.nth subgoals (which_subrule - 1) in
				instantiate (Atom (GOAL_VAR (which_subgoal, i1, j1))) _library
			with
			| Invalid_argument "List.nth" -> raise (Out_of_refinement_exception (standard_error_message "sound_interval_variable" ouf_of_refinement_error_message))
		)
	)
;;


(**-------------------- END INTERVAL VARIABLES -----------------------**)


(* checking whether an atom belongs to an atom set *)
let belongs (_atom: atom) (_atom_set: atom_set): bool =
	List.mem _atom _atom_set
;;

(* inclusion of an atom set in another *)
let rec subset (_atom_set_1: atom_set) (_atom_set_2: atom_set): bool =
	match _atom_set_1 with
	| [] -> true
	| _atom::other_atoms -> belongs _atom _atom_set_2 && subset other_atoms _atom_set_2
;;


(** NB: parameterized formulae are NOT authorized **)
let rec evaluate (valuation: atom_set) (_external_formula: external_formula): bool =
	match _external_formula with
	| Ext_True -> true
	| Ext_False -> false
	| Ext_Atom _atom -> belongs _atom valuation
	| Ext_Not _external_formula_1 -> not (evaluate valuation _external_formula_1)
	| Ext_And (_external_formula_1, _external_formula_2) -> (evaluate valuation _external_formula_1) && (evaluate valuation _external_formula_2)
	| Ext_Or (_external_formula_1, _external_formula_2) -> (evaluate valuation _external_formula_1) || (evaluate valuation _external_formula_2)
	| Ext_Implies (_external_formula_1, _external_formula_2) -> (not (evaluate valuation _external_formula_1)) || (evaluate valuation _external_formula_2)
;;


(* turn a refinement rule and a trace interval
	into a right-hand implication formula (formula psi),
	in agreement with the library and the trace *)
(* PRECONDITION: the refinement assocated with the rule is correct *)
(* NB: we could settle for instantiating the goals with rules only once the
	non-instantiated formulae have been built, but this is less optimised:
	we would process the formulae several times *)
(* /!\ À FAIRE : ne pas se préoccuper ici de la correction du raffinement, car regardé
dans la fonction principale via correct_refinement *)
(** /!\ À CORRIGER : cas TERMINAL_OP (adapter au bon type) ;
	utiliser "evaluate" pour l'évaluation d'une formule
	par une valuation (= atom_set) **)
let rec right_hand_formula (source_goal, (op, subgoals): refinement_rule) (start_index: int) (end_index: int) (_library: library) (_trace: trace): formula =

	match op with
	| TERMINAL_OP -> let [TERMINAL_GOAL (start_formula, end_formula)] = subgoals in
		begin
			if evaluate _trace.(start_index) start_formula
			&& evaluate _trace.(end_index) end_formula
			then True
			else False
		end
	| _ ->
		begin
		match subgoals with

		(*
		| [] -> failwith "right_hand_formula : règle sans sous-buts" (* we assume that the subgoals list is never empty *)
		*)

		| (NONTERMINAL_GOAL goal_1)::remaining_subgoals ->
			begin
				match op with

				(* /!\ INTERDIRE le OR unaire ou pas ? (cf. SAND) *)
				| OR_OP ->
						(* ancillary function: appending a variable through disjunction *)
						(* let append_disjunct_goal (_formula: formula) (rule: refinement_rule): formula =
							append_disjunct _formula (Atom (REFINEMENT_VAR (rule, start_index, end_index)))
						in *)
						let append_disjunct_goal (_formula: formula) (_goal: goal): formula =
							let NONTERMINAL_GOAL _nonterminal_goal = _goal in
							append_disjunct _formula (instantiate (Atom (GOAL_VAR (_nonterminal_goal, start_index, end_index))) _library)
						in
						(* we build the disjunction of the variables associated with the subgoals *)
						(* List.fold_left append_disjunct_goal (instantiate (Atom (GOAL_VAR (goal_1, start_index, end_index))) _library) remaining_subgoals *)
						List.fold_left append_disjunct_goal (instantiate (Atom (GOAL_VAR (goal_1, start_index, end_index))) _library) remaining_subgoals

				(* /!\ INTERDIRE le SAND unaire ou pas ? Si oui, le code en commentaire plus bas fonctionne *)
				(* NB: combinatorial explosion with SAND arity *)
				(* REMARK: associative decomposition; indeed, SAND (A, B, C) = SAND (A, SAND (B, C)) *)
				| SAND_OP ->
					begin
					match remaining_subgoals with
					| [] -> instantiate (Atom (GOAL_VAR (goal_1, start_index, end_index))) _library (*failwith "right_hand_formula : règle SAND avec un seul sous-but"*)

					(*
					| [goal_2] ->

					let conjunction_list = init_list (end_index - start_index + 1) (fun jump_index_incr -> And (Atom (goal_1, start_index, start_index + jump_index_incr), Atom (goal_2, start_index + jump_index_incr, end_index))) in
					list_to_disjunction conjunction_list
					*)

					| _ ->
					(** /!\ N.B. : sous-optimisé de composer avec "init_list" au lieu de passer directement par un "fold" ?
						Si oui, alors à modifier éventuellement. **)
					let conjunction_list = init_list (end_index - start_index + 1) (fun jump_index_incr -> And (instantiate (Atom (GOAL_VAR (goal_1, start_index, start_index + jump_index_incr))) _library, right_hand_formula (source_goal, (SAND_OP, remaining_subgoals)) (start_index + jump_index_incr) end_index _library _trace)) in
					list_to_disjunction conjunction_list
					end

					(* with interval variables *)
				| AND_OP ->
					(* borderline case equivalent to SAND processing: the decomposition *)
					(* /!\ N.B. : ATTENTION : marche si le SAND est programmé pour n'importe quelle arité ;
					sinon (SAND imposés binaires), code à réinjecter ici *)
					if start_index = end_index then
						right_hand_formula (source_goal, (SAND_OP, subgoals)) start_index end_index _library _trace
					else
					begin
					match remaining_subgoals with
					| [] -> instantiate (Atom (GOAL_VAR (goal_1, start_index, end_index))) _library
					| _ ->

						let arity = List.length subgoals
						and rule = (source_goal, (op, subgoals)) in
						And (
						(* first block from the conjunction *)
						generic_single_conjunction 1 arity (fun which_subrule -> unique_formula rule which_subrule start_index end_index)
						,
							And (
							(* second block from the conjunction *)
							generic_single_conjunction 1 arity (fun which_subrule -> sound_interval_variable rule which_subrule start_index end_index _library)
							,
							(* third block from the conjunction *)
							(* REMARK: in comparison to the article:
							 – "tile" matches alpha;
							 – "which_subrule" matches k;
							 – [|i1 ; j1|] is the interval I.
							 The condition over I amounts to allow i1 to
							 range from 1 [/!\ start_index instead of 1 !?] to tile
							 and j1 from tile + 1 to j (= end_index)
							 	[/!\ CE QUI SUIT au lieu de "j" (version d'avant) : NON ? : n (= Array.length _trace)].
							*)
							generic_single_conjunction
							start_index
							(end_index - 1)
							(fun tile ->
								generic_single_disjunction
								1
								arity
								(fun which_subrule ->
									generic_single_disjunction
									start_index(* 1 *) (* /!\ À CONFIRMER *)
									tile
									(fun i1 ->
										generic_single_disjunction
										(tile + 1)
										end_index(* (Array.length _trace) *) (* /!\ À CONFIRMER *)
										(fun j1 ->
											Atom (INTERVAL_VAR (rule, start_index, end_index, which_subrule, i1, j1))
										)
									)
								)
							)
							)
						)
					end

			end
		end
;;



(* turn a refinement rule and a trace interval
	into a formula (implication), in agreement with the library and the trace *)
let formula_of_rule (rule: refinement_rule) (start_index: int) (end_index: int) (_library: library) (_trace: trace): formula =
	Implies (Atom (REFINEMENT_VAR (rule, start_index, end_index)), right_hand_formula rule start_index end_index _library _trace)
;;
