(******************* UTILITIES ************************)

open Types
open Exceptions


(******** GLOBAL VARIABLES ********)

(* global association list for numbering goals *)
(* goal_numbers is an association list numbering each nonterminal goal *)
(* INVARIANT: goal_numbers is always sorted by decreasing index number;
	this is useful for adding a new goal, using a stack structure
*)
(* NB: the dummy initialisation value is syntactically necessary
	for type reasons, as goal_numbers is a reference
*)
let goal_numbers: ((nonterminal_goal * int) list ref) = ref [("", -1)];;


(* delimiter in rule names; this character is FORBIDDEN for goal names *)
(* NB: "@", "%", and "#" are not directly accepted by Touist. *)
let _CONST_rule_name_delimiter: char = '@';;

(******** END: GLOBAL VARIABLES ********)



(*
(* at least one right-hand goal; if terminal refinement, only one goal, which has to be terminal *)
let correct_refinement (op: operator) (gl: goal list): bool =
	match (op, gl) with
	| _, [] -> false (* 0-ary operators unauthorized *)
	| TERMINAL_OP, (NONTERMINAL_GOAL _)::_ -> false  (* a terminal refinement matches a terminal goal *)
	| TERMINAL_OP, _::_::_ -> false  (* a terminal refinement is unary *)
	| _ -> true
;;
*)

(* raise an exception with the corresponding message: name of the involved function and what kind of error *)
let standard_error_message (function_name: string) (exception_error_message: string) =
	function_name ^ ": " ^ exception_error_message
;;

(* verifying the well-formation of refinements *)
let correct_refinement ((op: operator), (gl: goal list): refinement): unit =
	match (op, gl) with

 	  (* 0-ary operators unauthorized *)
 	| _, [] -> raise (Zero_arity_exception (standard_error_message "correct_refinement" zero_arity_error_message))

	  (* a terminal refinement matches a terminal goal *)
	| TERMINAL_OP, (NONTERMINAL_GOAL _)::_ -> raise (Nonterminal_goal_exception (standard_error_message "correct_refinement" nonterminal_goal_error_message))

	  (* a terminal refinement is unary *)
	| TERMINAL_OP, _::_::_ -> raise (Not_unary_exception (standard_error_message "correct_refinement" "non-unary terminal refinement"))
	| _ -> ()
;;

(* /!\ pour écrire plus facilement, au moins pour vérification de correct_refinement ; inutile ailleurs ? *)
(* turns a list of ordered pairs of atom sets into a list of terminal goals *)
let make_terminal_goal_list (formulae_pair_list: (external_formula * external_formula) list): goal list =
	let goal_of_pair (initial_formula, final_formula) = TERMINAL_GOAL (initial_formula, final_formula) in
	List.map goal_of_pair formulae_pair_list
;;



(* constructor: creating a refinement variable *)
let create_refinement_variable (rule: refinement_rule) (start_index: int) (end_index: int): variable =
	REFINEMENT_VAR (rule, start_index, end_index)
;;


(* constructor: creating an interval variable *)
let create_interval_variable (rule: refinement_rule) (start_index: int) (end_index: int) (number: int) (substart_index: int) (subend_index: int): variable =
	INTERVAL_VAR (rule, start_index, end_index, number, substart_index, subend_index)
;;


(* converting an atom to a legible string *)
let string_of_atom (_atom: atom): string =
	_atom
;;


(** /!\ À UTILISER POUR AFFICHER LA TRACE **)
(* converting an atom set to a legible string *)
let string_of_atom_set (_atom_set: atom_set): string = match _atom_set with
	| [] -> "∅"
	| [_atom] -> string_of_atom _atom
	| _atom::other_atoms -> List.fold_left (fun current_string _atom -> current_string ^ ", " ^ (string_of_atom _atom)) (string_of_atom _atom) other_atoms
;;


(** /!\ À UTILISER DANS LE RENDU OU LES TESTS **)
let print_trace (_trace: trace): unit =
	Array.iter (fun _atom_set -> print_endline (string_of_atom_set _atom_set)) _trace
;;


(* converting an external formula to a legible string *)
let rec string_of_external_formula (_formula: external_formula): string =
	match _formula with
	| Ext_True -> "true"
	| Ext_False -> "false"
	| Ext_Atom atom_name -> atom_name
	| Ext_Not formula_1 -> "not " ^ (string_of_external_formula formula_1)
	| Ext_And (formula_1, formula_2) -> "(" ^ (string_of_external_formula formula_1) ^ " and " ^ (string_of_external_formula formula_2) ^ ")"
	| Ext_Or (formula_1, formula_2) -> "(" ^ (string_of_external_formula formula_1) ^ " or " ^ (string_of_external_formula formula_2) ^ ")"
	| Ext_Implies (formula_1, formula_2) -> "(" ^ (string_of_external_formula formula_1) ^ " => " ^ (string_of_external_formula formula_2) ^ ")"
;;



(** /!\ ANCIENNE VERSION : texte **)
(* (* NB: for encapsulation, in case of some type change *)
let string_of_goal (_goal: goal): string = match _goal with
	| NONTERMINAL_GOAL g -> g
	| TERMINAL_GOAL (formula_1, formula_2) ->
		"<"
		^ (string_of_external_formula formula_1)
		^ " | "
		^ (string_of_external_formula formula_2)
		^ ">"
;;
 *)


(* SIDE EFFECT: numbers the goal if not yet numbered *)
let number_of_goal (goal_name: nonterminal_goal): int =
	match List.assoc_opt goal_name !goal_numbers with
	| None ->
		let number = snd (List.hd !goal_numbers) + 1
		in goal_numbers := (goal_name, number)::!goal_numbers;
		number
	| Some number -> number
;;

(** /!\ NOUVELLE VERSION (en cours) : nombre pour Touist **)
(* NB: for encapsulation, in case of some type change *)
(* /!\ For Touist compatibility, we use a NUMBER instead of mere text. *)
let string_of_goal (_goal: goal): string = match _goal with
	| NONTERMINAL_GOAL goal_name -> string_of_int (number_of_goal goal_name)
	| TERMINAL_GOAL (formula_1, formula_2) ->
		"<"
		^ (string_of_external_formula formula_1)
		^ " | "
		^ (string_of_external_formula formula_2)
		^ ">"
;;



(* converting a refinement operator to a legible string *)
let string_of_operator (_operator: operator): string = match _operator with
	| OR_OP -> "OR"
	| SAND_OP -> "SAND"
	| AND_OP -> "AND"
	| TERMINAL_OP -> raise (Terminal_operator_exception (standard_error_message "string_of_operator" terminal_operator_error_message)) 
;;



(* /!\ ANCIENNE VERSION : avec texte au lieu de nombres, SANS liste d'association *)
(**
(* converting a refinement to a legible string *)
(* PRECONDITION: the refinement is correct *)
let string_of_refinement (_operator, subgoals: refinement): string =

	match _operator with
	| TERMINAL_OP -> let [terminal_goal] = subgoals in
		string_of_goal terminal_goal
	| _ ->
		begin
		match subgoals with

		(*
			NB: problematic cases are taken care of outside, in the main function.
		*)

		| first_goal::other_goals ->
			(string_of_operator _operator)
			^ "("
			^ (string_of_goal first_goal)
			^ (List.fold_left (fun current_string _goal -> current_string ^ ", " ^ (string_of_goal _goal)) "" other_goals)
			^ ")"
		end
;;
**)


(** /!\ NOUVELLE VERSION **)
(* converting a refinement to a legible string *)
(* PRECONDITION: the refinement is correct *)
let string_of_refinement (_operator, subgoals: refinement): string =

	match _operator with
	| TERMINAL_OP -> let [terminal_goal] = subgoals in
		string_of_goal terminal_goal
	| _ ->
		begin
		match subgoals with

		(*
			NB: problematic cases are taken care of outside, in the main function.
		*)

		| first_goal::other_goals ->
			(string_of_operator _operator)
			^ "("
			^ (string_of_goal first_goal)
			^ (List.fold_left (fun current_string _goal -> current_string ^ ", " ^ (string_of_goal _goal)) "" other_goals)
			^ ")"
		end
;;


(* displaying a refinement *)
let print_refinement (_refinement: refinement): unit =
	print_endline (string_of_refinement _refinement)
;;



(*
	/!\ système à améliorer pour alléger ? Car affiche toutes les règles ;
	plutôt réussir à les nommer (ex. : R1, R2…) ?
	Notion globale : à traiter globalement (via un tableau ou autre) ?
	Soit nommer automatiquement via le programme (mieux ?), soit rentrer les noms en paramètres.
*)
(* converting a refinement rule to a legible string *)
(* NB: we need the definition of string_of_refinement beforehand. *)
(** /!\ NB : homonymie entre flèches de réécriture et d'implication ;
	changer au moins l'une des deux ? Par exemple, "=>" pour implication ? **)
let string_of_rule (_goal, _refinement: refinement_rule): string =
	(string_of_goal (NONTERMINAL_GOAL _goal)) ^ " -> " ^ (string_of_refinement _refinement)
;;

(* /!\ A VOCATION À REMPLACER "string_of_rule" ? *)
(* /!\ LE NOM DOIT ÊTRE UN NOMBRE (chaîne dénotant un nombre), POUR TOUIST *)
let string_of_named_rule (_goal, _refinement: refinement_rule) (_named_library: named_library): string =
	let named_refinements = List.assoc _goal _named_library in
	List.assoc _refinement named_refinements
;;

(* string associated with a named refinement *)
(* NB: in fact, merely a specialisation of "snd", but written for encapsulation *)
let string_of_named_refinement (_named_refinement: named_refinement): string =
	snd _named_refinement
;;

let print_named_refinement (_named_refinement: named_refinement): unit =
	print_endline (string_of_named_refinement _named_refinement)
;;


(* associate a rule name to each in the library
	FORMAT: <goal name><refinement number, starting from 0>
	SIDE EFFECT: numbers all goals
*)
(* NB: not merely for display; but could be algorithmically expendable.
  Could enable reducing the complexity of rule filtering. *)
(* /!!!\ For Touist compatibility reasons, as mere text is NOT suitable,
	the name chosen here is a couple of integers (without parentheses),
	e.g. "5,3".
*)
(* /!\ À COMPLÉTER
    @param _library is ...
    @return ...
*)
let name_library (_library: library): named_library =
	let name_refinements (_goal: nonterminal_goal) (refinements: refinement list): (refinement * string) list =

		(** /!\ ANCIENNE VERSION avec texte **)
		(* 
		List.mapi (fun counter _refinement -> (_refinement, (string_of_goal (NONTERMINAL_GOAL _goal)) ^ (Char.escaped _CONST_rule_name_delimiter) ^ (string_of_int counter))) refinements
		 *)

		(** /!\ NOUVELLE VERSION avec couple de nombres **)
		List.mapi (fun counter _refinement -> (_refinement, (string_of_goal (NONTERMINAL_GOAL _goal)) ^ "," ^ (string_of_int counter))) refinements

	in

	let goal_numbers: ((nonterminal_goal * int) list ref) = ref [("", -1)]
	in List.map (fun (_goal, refinements) -> (_goal, name_refinements _goal refinements)) _library
;;


(* displaying a refinement rule *)
let print_rule (rule: refinement_rule): unit =
	print_endline (string_of_rule rule)
;;

(* displaying a named refinement rule *)
let print_named_rule (_goal: nonterminal_goal) (_named_refinement: named_refinement): unit =
	print_endline (
		string_of_named_refinement _named_refinement
		^ " : "
		^ (string_of_rule (_goal, fst _named_refinement))
	)
;;


(* displaying a library with named rules *)
let print_named_library (_library: named_library): unit =
	List.iter
		(fun (_goal, named_refinements) ->
			(
				List.iter
				(print_named_rule _goal)
				named_refinements
			)
		)
		_library;
	print_newline ();
;;


(* converting an internal SAT formula to a legible string *)
let rec string_of_formula (_formula: formula) (_library: named_library): string =
	match _formula with
	| False -> "⊥" (* /!\ Caractère antitruc : suffisamment adaptable aux divers systèmes ou risque de problème d'affichage ? *)
	| True -> "T"
 	| Atom _variable ->
 		begin
		match _variable with	
		| REFINEMENT_VAR (rule, _start, _end) ->
		 "x[" ^ (string_of_named_rule rule _library) ^ "](" ^ (string_of_int _start) ^ "," ^ (string_of_int _end) ^ ")"
		| INTERVAL_VAR (rule, _start, _end, number, _substart, _subend) ->
		 "y[" ^ (string_of_named_rule rule _library) ^ "," ^ (string_of_int _start) ^ "," ^ (string_of_int _end) ^ "](" ^ (string_of_int number) ^ "," ^ (string_of_int _substart) ^ "," ^ (string_of_int _subend) ^ ")"
		| GOAL_VAR (_goal, _start, _end) -> _goal (* /!\ TEMPORAIRE : cas anormal ? Ou à laisser pour formules en forme intermédiaire (non instanciée) ? *)
		end
 	| Not formula_0 -> "!" ^ (string_of_formula formula_0 _library)
	| And (formula_1, formula_2) -> "(" ^ (string_of_formula formula_1 _library) ^ " ^ " ^ (string_of_formula formula_2 _library) ^ ")"
	| Or (formula_1, formula_2) -> "(" ^ (string_of_formula formula_1 _library) ^ " v " ^ (string_of_formula formula_2 _library) ^ ")"
	| Implies (formula_1, formula_2) -> "(" ^ (string_of_formula formula_1 _library) ^ " -> " ^ (string_of_formula formula_2 _library) ^ ")"
;;


(* displaying a formula *)
let print_formula (_formula: formula) (_named_library: named_library): unit =
	print_endline (string_of_formula _formula _named_library)
;;


(* displaying a PIC pack *)
let rec display_pack (_pack: pack): unit =
	match _pack with
	| [] -> ()
	| (start_index, end_index)::intervals ->
		begin
			print_string ("[" ^ (string_of_int start_index) ^ "," ^ (string_of_int end_index) ^ "] "); 
			display_pack intervals;
		end
;;

(* displaying a PIC instance *)
let display_PIC (length, packs: pic_instance): unit =
	let rec display_packs (packs: pack list): unit =
		match packs with
		| [] -> ()
		| _pack::other_packs ->
			begin
				display_pack _pack;
				print_newline ();
				display_packs other_packs;
			end
	in
	print_endline ("Interval = [1, " ^ (string_of_int length) ^ "]");
	print_endline ("Packs:");
	display_packs packs;
	print_newline ();
;;
