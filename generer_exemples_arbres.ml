(** Generation of big trees whose type derives from a PIC instance,
for visual rendering purposes. **)


open Types
open Parsing


(* Parameter values examples *)
(* max_trace_length: 100
	max_width: 5
	max_leaves_number: 3
	examples_number: 10
*)


let generate_leaf (trace_length: int): tree =
	let debut = Random.int (trace_length + 1) in
	let fin = debut + (Random.int (trace_length + 1 - debut)) in

	TERMINAL_tree ((Ext_Atom "p", Ext_Atom "q"), ("gTERM", debut, fin))
;;

let rec generate_leaves (trace_length: int) (number_of_leaves: int): tree list =
	match number_of_leaves with
	| 0 -> failwith "generate_leaves: problem"
	| 1 -> [generate_leaf trace_length]
	| _ -> (generate_leaf trace_length)::(generate_leaves trace_length (number_of_leaves - 1))
;;

let generate_tree (trace_length: int) (max_leaves_number: int) (width: int): tree =
	let rec generate_subtrees (number_of_subtrees: int): tree list =
		match number_of_subtrees with
		| 0 -> failwith "generate_tree: problem"
		| 1 -> 
			let debut = Random.int (trace_length + 1) in
			let fin = debut + (Random.int (trace_length + 1 - debut)) in
			[OR_tree (generate_leaves trace_length (Random.int max_leaves_number + 1), ("gOR", debut, fin))]
		| _ -> 
			let debut = Random.int (trace_length + 1) in
			let fin = debut + (Random.int (trace_length + 1 - debut)) in
			(OR_tree (generate_leaves trace_length (Random.int max_leaves_number + 1), ("gOR", debut, fin)))
			::(generate_subtrees (number_of_subtrees - 1))
	in
	AND_tree (generate_subtrees width, ("gAND", 0, trace_length))
;;


print_endline "Maximal trace length:";;
let max_trace_length = int_of_string (read_line ());;
print_endline "Maximal tree width:";;
let max_width = int_of_string (read_line ());;
print_endline "Maximal number of leavers per OR:";;
let max_leaves_number = int_of_string (read_line ());;
print_endline "Number of files to be generated:";;
let examples_number = int_of_string (read_line ());;


(* The created files are named "example_<number>" and filed
under the "grands_arbres" subdirectory.
*)
Random.self_init ();
for iteration = 1 to examples_number do
	let trace_length = Random.int max_trace_length + 1
	and width = Random.int max_width + 1
	in
	let _tree = generate_tree trace_length max_leaves_number width
	in
	let format_JSON = json_of_output trace_length _tree
	and file = open_out ("./grands_arbres/example_" ^ (string_of_int iteration))
	in output_string file format_JSON	
done
;;
