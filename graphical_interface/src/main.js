/** function loaded once the page is loaded*/
$(document).ready(function() {
    $("#attackTree").on("keyup", update);
    update();
});

/* update the SVGs*/
function update() {
    let attackTreeInfo = eval("(" + $("#attackTree").val() + ")");
    createSVGTrace(attackTreeInfo.trace);
    createSVGTree(attackTreeInfo.tree);
}

/*
 @param trace: an array of states
 @effect: display the trace in $("#trace")
 */
function createSVGTrace(trace) {
    let height=32;
    
    function getX(i) {
        if(trace.length*height < $("#trace").width())
            return i*height;
        else
            return $("#trace").width() * i/trace.length;
    }
    
    function getWidth() {
        if(trace.length*height < $("#trace").width())
            return height;
        else
            return $("#trace").width() / trace.length;
    }
    
    $("#trace").empty();
    
    function SVG(tag) {
        return document.createElementNS('http://www.w3.org/2000/svg', tag);
    }

    function addState(i) {
        $(SVG('rect'))
            .attr('id', "state" + i) 
            .attr('x', getX(i))
            .attr('class', "state")
            .attr('y', 0)
            .attr('width', getWidth())
            .attr('height', height)
            .appendTo($("#trace"));
    }
    
    for(let i in trace)
        addState(i);
    
   
}



function noTraceHightlight() {
    $('.statehighlighted').attr('class', 'state');
}


/* @param data an object with d.begin and d.end */
function traceHighlight(data) {
    noTraceHightlight();
    
    for(let i = data.begin; i <= data.end; i++)
        $('#state' + i).attr('class', 'statehighlighted');
}

/*
 @param tree: an attack tree
 @effect: display the attack tree in $("#tree")
 */
function createSVGTree(treeData) {
    

    // Set the dimensions and margins of the diagram
    var margin = {top: 20, right: 90, bottom: 30, left: 90},
        width = 2000 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

        var orientations = {
    "top-to-bottom": {
        size: [width, height],
        x: function(d) { return d.x; },
        y: function(d) { return d.y; }
    },
    "right-to-left": {
        size: [height, width],
        x: function(d) { return width - d.y; },
        y: function(d) { return d.x; }
    },
    "bottom-to-top": {
        size: [width, height],
        x: function(d) { return d.x; },
        y: function(d) { return height - d.y; }
    },
    "left-to-right": {
        size: [height, width],
        x: function(d) { return d.y; },
        y: function(d) { return d.x; }
    }
    };

    var orientation = orientations["top-to-bottom"];

    d3.select("#tree").selectAll("*").remove();
    
    var svg = d3.select("#tree")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var i = 0,
        duration = 750, //duration of animation in ms
        root;

    // declares a tree layout and assigns the size
    var treemap = d3.tree().size([width/2, height]);

    // Assigns parent, children, height, depth
    root = d3.hierarchy(treeData, function(d) { return d.children; });
    root.x0 = width / 2;
    root.y0 = 0;


    updateFrom(root);



    function updateFrom(source) {

    var treeData = treemap(root);
    var nodes = treeData.descendants(),
        links = treeData.descendants().slice(1);
    nodes.forEach(function(d){ d.y = d.depth * 90});

    // ****************** Nodes section ***************************
    var node = svg.selectAll('g.node')
        .data(nodes, function(d) {return d.id || (d.id = ++i); });

    var nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr("transform", function(d) {
            return "translate(" + source.x0 + "," + source.y0 + ")";
        })
        .on('click', click)
        .on('mousemove', (d) => traceHighlight(d.data))
        .on('mouseout', noTraceHightlight);
        
    let nodeHeight = 24;
    let nodeWidth = 36;

    nodeEnter.append('rect')
        .attr('class', 'node')
        .attr('x', (d) => orientation.x(d) - 16)
        .attr('y', (d) => orientation.y(d)-16)
        .attr('height', nodeHeight)
        .attr('width', nodeWidth)
        .attr('class', (d) => d.data.type)
        .style("stroke", function(d) {
            return d._children ? "1px" : "0px";
        });

    nodeEnter.append('text')
    .attr('x', (d) => orientation.x(d) - 12)
        .attr('y', orientation.y)
        .text(function(d) { return d.data.goal; });

    var nodeUpdate = nodeEnter.merge(node);

    nodeUpdate//.transition()
        //.duration(duration)
        .attr("transform", function(d) { 
            return "translate(" + orientation.x(d) + "," + orientation.y(d) + ")";
        })
        
        

    // Update the node attributes and style
    nodeUpdate.select('rect')
        .style("stroke", "black")
        .style("stroke-width", (d) => d._children ? "1px" : "0px")
        .attr('cursor', 'pointer');


    // Remove any exiting nodes
    var nodeExit = node.exit().remove();

    nodeExit.select('text')
        .style('fill-opacity', 1e-6);

    // ****************** links section ***************************

    var link = svg.selectAll('path.link')
        .data(links, function(d) { return d.id; });

    var linkEnter = link.enter().insert('path', "g")
        .attr("class", "link")
        .attr('d', function(d){
            var o = {x: source.x0, y: source.y0}
            return diagonal(o, o)
        });

    var linkUpdate = linkEnter.merge(link);
    linkUpdate//.transition()
        //.duration(duration)
        .attr('d', function(d){
            return diagonal(d, d.parent) });

    var linkExit = link.exit().remove();

    nodes.forEach(function(d){
        d.x0 = d.x;
        d.y0 = d.y;
    });

    function diagonal(s, d) {
        let sy = orientation.y(s)*2;
        let sx = orientation.x(s)*2;
        let dy = orientation.y(d)*2;
        let dx = orientation.x(d)*2;

        path = `M ${sx} ${sy}
                C ${(sx + dx) / 2} ${sy},
                ${(sx + dx) / 2} ${dy},
                ${dx} ${dy}`

        return path;
    }

    function click(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        updateFrom(root);
    }
    

    nodesCloseAll = function() {
        nodes.forEach( function(d) {
            if(d.children) {
                d._children = d.children;
                d.children = null;
            }
            updateFrom(root);
        });
    } 
    
    $("#nodesCloseAll").on("click", nodesCloseAll);
}
}

