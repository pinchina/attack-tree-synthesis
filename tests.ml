(******************* TESTS ************************)
IFDEF TEST THEN

open Types
open Exceptions
open Utilities
open Formulae
open Synthese
open Test_generation
open Parsing

(* Function correct_refinement *)

(* must be accepted *)
correct_refinement (TERMINAL_OP, (make_terminal_goal_list [(Ext_Atom "a", Ext_Atom "b")]));;
print_endline "correct_refinement : correct";;

(* must not be accepted *)
try
correct_refinement (TERMINAL_OP, (make_terminal_goal_list []))
with
Zero_arity_exception error_message -> print_endline error_message;;

(* must not be accepted *)
try
correct_refinement (TERMINAL_OP, [NONTERMINAL_GOAL "g"])
with
Nonterminal_goal_exception error_message -> print_endline error_message;;

(* must not be accepted *)
try
goal_numbers := [("b", 1) ; ("a", 0) ; ("", -1)];
correct_refinement (TERMINAL_OP, (make_terminal_goal_list [Ext_And (Ext_Atom "a", Ext_Atom "b"), Ext_And (Ext_Atom "a", Ext_Atom "b")]));
with
Not_unary_exception error_message -> print_endline error_message;;


(* Display functions *)

goal_numbers := [("g", 3) ; ("c", 2) ; ("b", 1) ; ("a", 0) ; ("", -1)];
let _refinement = (SAND_OP, [NONTERMINAL_GOAL "a"; NONTERMINAL_GOAL "b"; NONTERMINAL_GOAL "c"]) in
let rule = "g", _refinement in
print_refinement _refinement;
print_rule rule
;;



(* /!\ À DÉPLACER DANS LA SECTION DE VÉRIFICATION, POUR ÉVITER LA COMPILATION EN GÉNÉRAL ? *)
(* veryfing function: displaying the propositional formula
	associated with a refinement *)
(* PARAMETERS:
"test_name": name of the test, merely for display purposes
"test_library": library (with unnamed rules)
	do not appear on the left side
"test_trace": trace
"source_goal": non-terminal goal being refined
"_refinement": a refinement associated with goal "source_goal";
	the associated rule is that of the formula under verification
"start_index": start index associated with the formula
"end_index": end index associated with the formula
 *)
let formula_test (test_name: string) (test_library: library) (test_trace: trace) (source_goal: nonterminal_goal) (_refinement: refinement) (start_index: int) (end_index: int): unit =

	print_endline test_name;

	print_endline "Bibliothèque de raffinement :";
	let named_test_library = name_library test_library in
	print_named_library named_test_library;

	print_refinement _refinement;

	print_endline ("entre " ^ (string_of_int start_index) ^ " et " ^ (string_of_int end_index) ^ " :");

	let test_formula = right_hand_formula (source_goal, _refinement) start_index end_index test_library test_trace in
	print_formula test_formula named_test_library;

	print_newline ();

;;

(* OR test *)
formula_test
"Formule OR :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ]
]
[||]
"source_goal"
(OR_OP, [NONTERMINAL_GOAL "a"])
0
0
;;

(* OR test *)
formula_test
"Formule OR :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ];
	"b", [(SAND_OP, [NONTERMINAL_GOAL "g1" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"c", [(AND_OP, [NONTERMINAL_GOAL "g2" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"d", [
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)]);
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_Atom "p1", Ext_And (Ext_Atom "p2", Ext_Atom "p3"))])
		 ]
]
[||]
"source_goal"
(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])
0
3
;;

(* SAND test *)
formula_test
"Formule SAND :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ]
]
[||]
"source_goal"
(SAND_OP, [NONTERMINAL_GOAL "a"])
0
0
;;

(* SAND test *)
formula_test
"Formule SAND :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ];
	"b", [(SAND_OP, [NONTERMINAL_GOAL "g1" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])]
]
[||]
"source_goal"
(SAND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b"])
0
1
;;

(* SAND test *)
formula_test
"Formule SAND :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ];
	"b", [(SAND_OP, [NONTERMINAL_GOAL "g1" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"c", [(AND_OP, [NONTERMINAL_GOAL "g2" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])]
]
[||]
"source_goal"
(SAND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c"])
0
2
;;

(* SAND test *)
formula_test
"Formule SAND :"
[
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ];
	"b", [(SAND_OP, [NONTERMINAL_GOAL "g1" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"c", [(AND_OP, [NONTERMINAL_GOAL "g2" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"d", [
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)]);
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_Atom "p1", Ext_And (Ext_Atom "p2", Ext_Atom "p3"))])
		 ]
]
[||]
"source_goal"
(SAND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])
0
3
;;

(**
(* test for formula uniq (for AND) *)
formula_test
"Formule uniq :"
[]
[||]
"source_goal"
(unique_formula ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 2 1 3)
0
0
;;
**)

let test_library = ["a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])]]
in let test_named_library = name_library test_library
in
print_endline "Formule uniq :";
print_named_library test_named_library;
print_formula
	(unique_formula ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 2 1 3)
	test_named_library
;;
print_newline ();


(* /!\ NB: case with equal start and end indices: not supposed to occur? *)
let test_library = ["a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])]]
in let test_named_library = name_library test_library
in
print_endline "Formule uniq :";
print_named_library test_named_library;
print_formula
	(unique_formula ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 2 1 1)
	test_named_library
;;
print_newline ();


(* must not be accepted *)
try
	let test_library = ["a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])]]
	in let test_named_library = name_library test_library
	in
	print_endline "Formule consistance :";
	print_named_library test_named_library;
	print_formula
		(sound_interval_variable ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 0 1 3 test_library)
		test_named_library
with
| Out_of_refinement_exception error_message -> print_endline error_message


let test_library = [
		"a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])];
		"f", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)])]
	]
in let test_named_library = name_library test_library
in
print_endline "Formule consistance :";
print_named_library test_named_library;
print_formula
	(sound_interval_variable ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 2 1 3 test_library)
	test_named_library
;;
print_newline ();


let test_library = [
		"a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])];
		"f", [
				(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)]);
				(SAND_OP, [NONTERMINAL_GOAL "A" ; NONTERMINAL_GOAL "B"]);
			]
	]
in let test_named_library = name_library test_library
in
goal_numbers := ("A",4)::("B",5)::!goal_numbers;
print_endline "Formule consistance :";
print_named_library test_named_library;
print_formula
	(sound_interval_variable ("a", (AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])) 2 1 3 test_library)
	test_named_library
;;
print_newline ();


(* AND test *)
let test_library = ["a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])]]
in
formula_test
"Formule AND :"
test_library
[||]
"source_goal"
(AND_OP, [NONTERMINAL_GOAL "a"])
0
0
;;

(** /!\ TEST NE PASSANT PAS ; À VOIR **)
(**
(* AND test *)
let test_library = [
		"a", [(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])];
		"b", [(SAND_OP, [NONTERMINAL_GOAL "g" ; NONTERMINAL_GOAL "h" ; NONTERMINAL_GOAL "i"])];
		"e", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_Atom "p1", Ext_And (Ext_Atom "p2", Ext_Atom "p3"))])];
		"f", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)])];
		"g", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)])];
		"h", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)])];
		"i", [(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)])];
	]
in
formula_test
"Formule AND :"
test_library
[||]
"source_goal"
(AND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b"])
0
1
;;
**)

(*(* AND test *)
formula_test
"Formule AND :"
(AND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c"])
0
2
;;*)

(*(* AND test *)
formula_test
"Formule AND :"
(AND_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])
0
3
;;*)

(* for the example PIC instance from the article *)
print_endline "Conversion PIC -> ATS :";
let _pic_instance =
(
	10,
	[
		[(1,2) ; (6, 10)];
		[(2,4) ; (1, 3)];
		[(4,7)];
		[(2,3) ; (3, 4) ; (7, 10)]
	]
)
in
display_PIC _pic_instance;
let (_ATS_trace, _ATS_library) =
_ATS_of_PIC _pic_instance
in
	let _ATS_named_library = name_library _ATS_library
	in
	print_endline "Instance ATS :";
	print_endline "1) Trace :";
	print_trace _ATS_trace;
	print_newline ();
	print_endline "2) Bibliothèque (nommée) :";
	print_named_library _ATS_named_library;
;;

(* for a random PIC instance: *)
print_endline "Conversion PIC -> ATS :";
let interval_length = 5
and number_of_packs = 3
and max_pack_size = 4
in
let _pic_instance = generate_PIC interval_length number_of_packs max_pack_size
in
print_endline "Instance PIC :";
display_PIC _pic_instance;
let (_ATS_trace, _ATS_library) =
_ATS_of_PIC _pic_instance
in
	let _ATS_named_library = name_library _ATS_library
	in
	print_endline "Instance ATS :";
	print_endline "1) Trace :";
	print_trace _ATS_trace;
	print_newline ();
	print_endline "2) Bibliothèque (nommée) :";
	print_named_library _ATS_named_library;
;;


let _PIC_synthesis_test (test_name: string) (_PIC_input: pic_instance): unit =
	print_endline "Synthèse :";
	print_endline test_name;

	let (input_trace, input_library) = _ATS_of_PIC _PIC_input
	in
	let _named_library = name_library input_library
	in

	print_endline "Instance ATS :";
	print_endline "1) Trace :";
	print_trace input_trace;
	print_newline ();
	print_endline "2) Bibliothèque (nommée) :";
	print_named_library _named_library;

	let result_tree = synthesize input_trace input_library
	in
	print_endline ("Result tree:");
	print_endline (json_of_tree result_tree);
;;

_PIC_synthesis_test
	"Petit exemple :"
	(
		2,
		[
			[(2,2) ; (1,1)];
			[(1,1)]
		]
	)
;;


_PIC_synthesis_test
	"Exemple PIC de l'article :"
	(
		10,
		[
			[(1,2) ; (6, 10)];
			[(2,4) ; (1, 3)];
			[(4,7)];
			[(2,3) ; (3, 4) ; (7, 10)]
		]
	)
;;

(* /!\ Version aléatoire *)
(*
let interval_length = 2
and number_of_packs = 2
and max_pack_size = 2
in
let _PIC_input = generate_PIC interval_length number_of_packs max_pack_size
_PIC_synthesis_test
	"Instance aléatoire :"
	_PIC_input
;;
*)


let input_synthesis_test (test_name: string) (filename: string): unit =
	print_endline "Synthèse :";
	print_endline test_name;

	let (input_trace, input_library) = input_of_file filename
	in
	let _named_library = name_library input_library
	in

	print_endline "Instance ATS :"; 
	print_endline "1) Trace :";
	print_trace input_trace;
	print_newline ();
	print_endline "2) Bibliothèque (nommée) :";
	print_named_library _named_library;

	let result_tree = synthesize input_trace input_library
	in
	print_endline ("Result tree:");
	print_endline (json_of_tree result_tree);
;;

(** /!\ Avec SAND non binaire **)
(* 
let museum_filename = "./museum/museum_1"
in
input_synthesis_test
	"Exemple musée de l'article :"
	museum_filename
;;
 *)
(** modified file **)
(*
let museum_filename = "./museum/museum_2"
in
input_synthesis_test
	"Exemple musée modifié (que binaire) :"
	museum_filename
;;
*)

(* binarised museum example *)
let museum_filename = "./museum/museum_3"
in
input_synthesis_test
	"Exemple musée binarisé :"
	museum_filename
;;


(* small example *)
(* let museum_filename = "./museum/small"
in
input_synthesis_test
	"Exemple arbitraire :"
	museum_filename
;;
 *)


name_library [
	"a", [
			(OR_OP, [NONTERMINAL_GOAL "a" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"]);
			(AND_OP, [NONTERMINAL_GOAL "e" ; NONTERMINAL_GOAL "f"])
		 ];
	"b", [(SAND_OP, [NONTERMINAL_GOAL "g1" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"c", [(AND_OP, [NONTERMINAL_GOAL "g2" ; NONTERMINAL_GOAL "b" ; NONTERMINAL_GOAL "c" ; NONTERMINAL_GOAL "d"])];
	"d", [
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_True, Ext_True)]);
			(TERMINAL_OP, [TERMINAL_GOAL (Ext_Atom "p1", Ext_And (Ext_Atom "p2", Ext_Atom "p3"))])
		 ]
];;

END
