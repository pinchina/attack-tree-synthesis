(************* RUN TIME TESTING ***************)

(******************* MODULES ************************)
open Synthese
open Exceptions
open Parsing
(******************* END MODULES ************************)


(******************* GLOBAL VARIABLES ************************)
let tree_exists = ref true;;

print_endline "Enter name of result file (/!\\ file will be overwritten):";;
let result_name = read_line ();;
let result_file = open_out result_name;;
(******************* END GLOBAL VARIABLES ************************)


(* takes the name of a file for ATS as input and runs a synthesis *)
let run_test (filename: string) (test_number: int) (number_of_tests: int): unit =

	let (instance_data, input_trace, input_library) = input_of_file_with_data filename
	in

	print_int test_number;
	print_char '/';
	print_int number_of_tests;
	print_endline (" (" ^ filename ^ "): ");

	tree_exists := true;
	let previous_time = Sys.time () in

	(********** ONE SYNTHESIS TEST **********)
	begin
	try
		let _ = synthesize input_trace input_library in ()
	with
	| No_tree_exception -> tree_exists := false
	end;
	(********** END: ONE SYNTHESIS TEST **********)

	let current_time = Sys.time () in

	(* used to ensure that the three phases match enough the total time;
	there can be some left out for minor operations, but not too much *)
	let run_time = current_time -. previous_time
	and checksum = !formula_time +. !solving_time +. !guiding_time in
	let delta = run_time -. checksum in

	let display_string =
		"Size Trace AND OR: " ^ instance_data
		^ "\n" ^ "Run time (T): " ^ string_of_float run_time
		^ "\n" ^ "SAT: " ^ string_of_bool !tree_exists
		^ "\n" ^ "Building (T1): " ^ string_of_float !formula_time
		^ "\n" ^ "Solving (T2): " ^ string_of_float !solving_time
		^ "\n" ^ "Guiding (T3): " ^ string_of_float !guiding_time
		^ "\n" ^ "Δ = T - (T1 + T2 + T3): " ^ string_of_float delta
		^ "\n"
	and result_string =
		instance_data
		^ " " ^ string_of_float run_time
		^ " " ^ string_of_bool !tree_exists
		^ " " ^ string_of_float !formula_time
		^ " " ^ string_of_float !solving_time
		^ " " ^ string_of_float !guiding_time
		^ " " ^ string_of_float delta
		^ "\n"
	in

	(** write results in a file **)
	output_string result_file result_string;

	(** displaying result in the terminal **)
	print_endline display_string;
	print_endline result_string;
;;


(******************* MAIN SECTION ************************)
print_endline "Enter directory for test files (/!\\ with ending /):";;
let directory = read_line ();;

let temp_file = Filename.temp_file "" "" in
(* only get files, neither directories nor subfiles *)
Sys.command ("find " ^ directory ^ " -maxdepth 1 -type f > " ^ temp_file);
let files = read_file temp_file
in
(* notice "find" already gives the directory in each file name *)
List.iteri (fun test_number filename -> run_test filename (test_number + 1) (List.length files)) files;;
(******************* END MAIN SECTION ************************)
