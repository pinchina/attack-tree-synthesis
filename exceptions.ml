(******************* EXCEPTIONS ************************)

exception Zero_arity_exception of string
let zero_arity_error_message = "0-ary operator";;

exception Nonterminal_goal_exception of string
let nonterminal_goal_error_message = "non-terminal goal";;

exception Not_unary_exception of string
let not_unary_error_message = "not unary";;

exception Terminal_operator_exception of string
let terminal_operator_error_message = "terminal operator";;

exception No_goal_exception of string
let no_goal_error_message = "goal not found in the library";;

exception Empty_list of string
let empty_list_error_message = "empty list";;

exception Out_of_refinement_exception of string
let ouf_of_refinement_error_message = "index does not refer to any subgoal";;

exception Bad_library_exception of string
let bad_library_error_message (delimiter: char) = "badly written library (wrong number of" ^ (String.make 1 delimiter) ^ ")";;

exception Bad_refinement_exception of string
let bad_refinement_error_message (delimiter: char) = "badly written refinement (wrong number of" ^ (String.make 1 delimiter) ^ ")";;

exception Bad_operator_name_exception of string
let bad_operator_name_error_message = "bad operator name";;

exception No_tree_exception
