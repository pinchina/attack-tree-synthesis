(******************* PARSING ************************)

(** File parsing, external formulae parsing, tree parsing. **)


open Types
open Exceptions
open Utilities


(******** GLOBAL VARIABLES ********)

(* delimiter between elements (atoms, subgoals...) in input format *)
let _CONST_element_delimiter: char = ' ';;
(* delimiter between atom sets in input format *)
let _CONST_atom_set_delimiter: char = ',';;
(* delimiter between a goal and its decomposition in input format *)
let _CONST_rule_delimiter: char = ':';;

(* generic character for refinement variables *)
let _CONST_refinement_variable: char = 'x';;
(* generic character for interval variables *)
let _CONST_interval_variable: char = 'y';;

(******** END: GLOBAL VARIABLES ********)


(* returns the list of lines from a text file, in their original order *)
let read_file (filename: string): string list = 
let lines = ref [] in
let chan = open_in filename in
try
  while true; do
    lines := input_line chan :: !lines
  done; !lines
with End_of_file ->
  close_in chan;
  List.rev !lines ;;


(* parsing a valuation lines/strings *)
(* NB: a Touist valuation line has the following format: "<0 or 1> <atom name>" *)
let rec valuation_of_strings (strings: string list): valuation =
	match strings with
	| [] -> []
	| mapping_string::other_strings ->
		(String.sub mapping_string 2 ((String.length mapping_string) - 2),  (* variable name *)
		Char.equal mapping_string.[0] '1')  (* variable evaluation *)
			::(valuation_of_strings other_strings)
	(** /!\ utiliser un truc global pour connaître le type de la variable ?
		Ainsi, sans typer les variables dans la valuation, retrouvable via non-homonymie ?? **)
;;


(* converting a formula to Touist format *)
let rec format_of_formula (_library: named_library) (input_formula: formula): string =
	match input_formula with
	| True -> "Top"
	| False -> "Bot"
	| Atom _variable ->
		begin
			match _variable with
			
			(** NB: indices could be letters (parameters) instead of numerical constants
			in the future, hence adding a "$" for Touist then **)
			| REFINEMENT_VAR (rule, start_index, end_index) ->
				(String.make 1 _CONST_refinement_variable) ^ "(" ^ (string_of_named_rule rule _library)
				^ "," ^ (string_of_int start_index)
				^ "," ^ (string_of_int end_index)
				^ ")"
			| INTERVAL_VAR (rule, start_index, end_index, which_subrule, substart_index, subend_index) ->
				(String.make 1 _CONST_interval_variable) ^ "(" ^ (string_of_named_rule rule _library)
				^ "," ^ (string_of_int start_index)
				^ "," ^ (string_of_int end_index)
				^ "," ^ (string_of_int which_subrule)
				^ "," ^ (string_of_int substart_index)
				^ "," ^ (string_of_int subend_index)
				^ ")"			
			| GOAL_VAR (_, _, _) -> failwith "format_of_formula: uninstantiated goal variable in some formula"
		end
	| Not _formula -> "not (" ^ (format_of_formula _library _formula) ^ ")"
	| And (_formula_1, _formula_2) -> "(" ^ (format_of_formula _library _formula_1) ^ " and " ^ (format_of_formula _library _formula_2) ^ ")"
	| Or (_formula_1, _formula_2) -> "(" ^ (format_of_formula _library _formula_1) ^ " or " ^ (format_of_formula _library _formula_2) ^ ")"
	| Implies (_formula_1, _formula_2) -> "(" ^ (format_of_formula _library _formula_1) ^ " => " ^ (format_of_formula _library _formula_2) ^ ")"
;;


(* matching a string with its internal variable type *)
(* NB: this ancillary function is defined for encapsulation purposes;
	in particular, if the generic variable names become strings
	that are longer than one character, then it needs a change
	to parse the string correctly
*)
(** /!\ Useless if "variable_of_format" detects variable type
	using the number of arguments as now, but this could change.
**)
(* 
let type_of_format (variable_string: string): int =
	let test_character = variable_string.[0] in
	if test_character = _CONST_refinement_variable then 0
	else if test_character = _CONST_interval_variable then 1
	else failwith "bad internal variable name"
;;
 *)


 (* extract the element from a list at a specified position *)
let rec nth_element (number: int) (_list: 'a list): 'a =
	match number with
	| 0 -> List.hd _list
	| _ -> nth_element (number - 1) _list
;;


(***
(** /!\ ANCIENNE VERSION : avec nom de règle textuel **)
(* find the internal representation of a rule using its display name *)
let find_rule (rule_name: string) (_library: named_library): refinement_rule =
	let [(goal_name: string); (refinement_number_string: string)] = String.split_on_char _CONST_rule_name_delimiter rule_name in
	let refinement_number: int = int_of_string refinement_number_string in
	let rec find_goal_and_refinement (_library: named_library): refinement_rule =
		match _library with
		| [] -> failwith "find_rule: rule not found in the library"
		| (_goal, goal_refinements)::remaining_library -> 
			if String.equal goal_name _goal && refinement_number < List.length goal_refinements
			then (_goal, fst (nth_element refinement_number goal_refinements))
			else find_goal_and_refinement remaining_library
	in
	find_goal_and_refinement _library
;;
***)

(* find the internal representation of a rule using its display name *)
let rec find_rule (goal_number: int) (refinement_number: int) (_named_library: named_library): refinement_rule =
	match _named_library with
	| [] -> failwith "find_rule: rule not found in the library"
	| (_goal, goal_refinements)::remaining_library ->
		begin
			match List.assoc_opt _goal !goal_numbers with
			| None -> failwith ("find_rule: goal " ^ _goal ^ " is not numbered") (* NB: this check is not necessary, but useful in case there is an error (a lack) in the goal numbering *)
			| Some number -> if number = goal_number && refinement_number < List.length goal_refinements
			then (_goal, fst (nth_element refinement_number goal_refinements))
			else find_rule goal_number refinement_number remaining_library		
		end
;;


(***
(** /!\ ANCIENNE VERSION : avec nom de règle textuel **)
(* converting a Touist variable to an internal variable *)
let variable_of_format (variable_string: string) (_library: named_library): variable =
	let prefix_length = 1 + String.length "(" (* 1 is for the generic letter of the variable *)
	and suffix_length = String.length ")" in
	match String.split_on_char ',' (String.sub variable_string prefix_length (String.length variable_string - 1 - suffix_length)) with
	| [rule_name; start_index; end_index] -> REFINEMENT_VAR (find_rule rule_name _library, int_of_string start_index, int_of_string end_index)
	| [rule_name; start_index; end_index; which_subrule; substart_index; subend_index] -> INTERVAL_VAR (find_rule rule_name _library, int_of_string start_index, int_of_string end_index, int_of_string which_subrule, int_of_string substart_index, int_of_string subend_index)
	| _ -> failwith "variable_of_format: badly formatted variable string"
;;
***)


(** /!\ ANCIENNE VERSION : avec nom de règles textuel **)
(**
(* converting a Touist variable to an internal variable *)
let variable_of_format (variable_string: string) (_named_library: named_library): variable =
	let prefix_length = 1 + String.length "(" (* 1 is for the generic letter of the variable *)
	and suffix_length = String.length ")" in
	match String.split_on_char ',' (String.sub variable_string prefix_length (String.length variable_string - 1 - suffix_length)) with
	| [goal_number; refinement_number; start_index; end_index] -> REFINEMENT_VAR (find_rule goal_number refinement_number _named_library, int_of_string start_index, int_of_string end_index)
	| [goal_number; refinement_number; start_index; end_index; which_subrule; substart_index; subend_index] -> INTERVAL_VAR (find_rule goal_number refinement_number _named_library, int_of_string start_index, int_of_string end_index, int_of_string which_subrule, int_of_string substart_index, int_of_string subend_index)
	| _ -> failwith "variable_of_format: badly formatted variable string"
;;
**)

(* converting a Touist variable to an internal variable *)
(* notice the first two strings (integers) define the associated refinement rule *)
let variable_of_format (variable_string: string) (_named_library: named_library): variable =
	let prefix_length = 1 + String.length "(" (* 1 is for the generic letter of the variable *)
	and suffix_length = String.length ")" in
	match String.split_on_char ',' (String.sub variable_string prefix_length (String.length variable_string - prefix_length - suffix_length)) with
	| [goal_number; refinement_number; start_index; end_index] -> REFINEMENT_VAR (find_rule (int_of_string goal_number) (int_of_string refinement_number) _named_library, int_of_string start_index, int_of_string end_index)
	| [goal_number; refinement_number; start_index; end_index; which_subrule; substart_index; subend_index] -> INTERVAL_VAR (find_rule (int_of_string goal_number) (int_of_string refinement_number) _named_library, int_of_string start_index, int_of_string end_index, int_of_string which_subrule, int_of_string substart_index, int_of_string subend_index)
	| _ -> failwith "variable_of_format: badly formatted variable string"
;;



(* converting a list of strings to a single string with line breaks *)
(* used to form a correct input file for Touist *)
let rec string_of_lines (formatted_list: string list): string =
	match formatted_list with
	| [] -> ""
	| formatted_line::other_lines -> formatted_line ^ "\n" ^ (string_of_lines other_lines)
;;



(******** INPUT ********)

(** Handling of input file; the format of said input file goes as follows:
	1) Trace: <atom> ... <atom>, ..., <atom> ... <atom>;
	2) Library: for each line, one of the two following formats:
		2.1) <goal> -> <OP> <goal> ... <goal>
		2.2) <goal> -> <formula>, <formula>
	NB: in both entries (trace and non-terminal rule):
		nothing if empty set; for example: g -> [, p q].
**)



(* ancillary function: deleting empty strings among a list *)
let delete_empty_strings (string_list: string list): string list =
	List.filter (fun _string -> not (String.equal _string "")) string_list
;;

(* parsing the elements from a string by removing the delimiter
wherever it occurs *)
(* NB: for a format where the element delimiter is supposed
  to be duplicable any number of times
*)
(* Remark: a more optimized version could consist in using
	deleting delimiters character-wise while scanning the string.
*)
let elements_of_string (element_string: string): string list =
	let split_string = String.split_on_char _CONST_element_delimiter element_string in
	delete_empty_strings split_string
;;

(* converting the input string into a trace *)
(** /!\ AJOUTER un String.trim vers le début ? **)
let trace_of_string (trace_input: string): trace =	
	let atom_sets = String.split_on_char _CONST_atom_set_delimiter trace_input in
	let trace_as_list: atom_set list = List.map elements_of_string atom_sets in
	Array.of_list trace_as_list
;;


(* converting a non-terminal refinement string into a proper refinement *)
let non_terminal_refinement_of_string (refinement_string: string): refinement =
	match elements_of_string refinement_string with
	| operator_name::subgoals ->
		begin
			match operator_name with
			| "OR" -> (OR_OP, List.map (fun subgoal -> NONTERMINAL_GOAL subgoal) subgoals)
			| "SAND" -> (SAND_OP, List.map (fun subgoal -> NONTERMINAL_GOAL subgoal) subgoals)
			| "AND" -> (AND_OP, List.map (fun subgoal -> NONTERMINAL_GOAL subgoal) subgoals)
			| _ -> raise (Bad_operator_name_exception (standard_error_message "non_terminal_refinement_of_string" bad_operator_name_error_message))
		end
	| _ -> raise (Bad_refinement_exception (standard_error_message "non_terminal_refinement_of_string" (bad_refinement_error_message _CONST_element_delimiter)))
;;


(*** /!\ POUR LE MOMENT : seulement prise en compte du cas atomique,
	qui suffit pour les instances PIC ;
	aussi, conjonction de deux atomes pour l'exemple du musée
***)
(* converting/parsing a string to an external propositional formula *)
(** /!\ préciser que formules non paramétrées ?? **)
let formula_of_string (formula_string: string): external_formula =

	let elements = elements_of_string formula_string in
	match elements with
	| [] -> failwith "evaluate: formula syntax error"
	| ["true"] -> Ext_True
	| ["false"] -> Ext_False
	| [atom_name] -> Ext_Atom atom_name
	(** /!\ À COMPLÉTER ; le dernier cas ci-dessus : suffit pour les
	instances PIC **)
	| [atom_name_1; "and"; atom_name_2] -> Ext_And (Ext_Atom atom_name_1, Ext_Atom atom_name_2)
	| [atom_name_1; "or"; atom_name_2] -> Ext_Or (Ext_Atom atom_name_1, Ext_Atom atom_name_2)
	(** /!\ Ajout des cas "and" et "or" avec deux atomes pour l'exemple du musée **)
;;


(* converting a refinement string into a proper refinement *)
(** /!\ À ÉCRIRE ; simple copie de atom_set_of_string, pour l'instant **)
(** /!\ TESTER en séparant/découpant via virgule :
un élément si but non terminal, deux si terminal, erreur sinon **)
let refinement_of_string (refinement_string: string): refinement =
	match String.split_on_char _CONST_atom_set_delimiter refinement_string with
	| [_] -> non_terminal_refinement_of_string refinement_string
	| [initial_formula_string; final_formula_string] -> (TERMINAL_OP, [TERMINAL_GOAL (formula_of_string initial_formula_string, formula_of_string final_formula_string)])
	| _ -> raise (Bad_refinement_exception (standard_error_message "refinement_of_string" (bad_refinement_error_message _CONST_atom_set_delimiter)))
(* 	let split_string = String.split_on_char _CONST_element_delimiter refinement_string in
	List.filter (fun _atom -> not (String.equal _atom "")) split_string *)
;;



(* adding a rule to a library *)
(* NB: a library is a pseudo/factorised association list, so we do not append
	a new goal entry to the library if the goal is already present; instead,
	we append the refinement to its refinement list *)
(* NB: ancillary function for library_of_strings; useful to avoid a double parse,
	as here, we simultaneously check whether the goal is already known
	and update
*)
let rec add_rule (main_goal: nonterminal_goal) (_refinement: refinement) (_library: library): library =
	match _library with
	| [] -> (main_goal, [_refinement])::_library
	| (_goal, refinements)::other_rules ->
		if String.equal main_goal _goal
		then (main_goal, _refinement::refinements)::other_rules
		else (_goal, refinements)::(add_rule main_goal _refinement other_rules)
;;


(* converting an input library to an internal library *)
let library_of_strings (library_input: string list): library =

	(* NB: current_library is an accumulator; library_of_strings is called
		with [] as the argument for this parameter.
	*)
	let rec library_of_strings_aux (library_input: string list) (current_library: library): library =
	match library_input with
	| [] -> current_library
	| line::next_lines ->
		match String.split_on_char _CONST_rule_delimiter line with
		| [main_goal_string; decomposition_string] ->
			begin
				let main_goal = String.trim main_goal_string
				and decomposition = String.trim decomposition_string in
				let _refinement = refinement_of_string decomposition in
				let new_library = add_rule main_goal _refinement current_library in
				library_of_strings_aux next_lines new_library

				(** /!\ CI-DESSOUS : conservé par sécurité, mais à SUPPRIMER
					si la version actuelle fonctionne
				**)
				(*
				match List.assoc_opt main_goal current_library with
					| None -> let new_rule = (main_goal, [_refinement]) in
						new_rule::(library_of_strings_aux next_lines (new_rule::current_library))
					| Some current_refinements ->
						begin
						 	 (** /!\ COMPLÉTER : ajouter ce nouveau raffinement
						 	 aux raffinements précédents du but 
						 	 OPTIMISATION : utiliser une fonction auxiliaire (de
						 	 mise à jour de cette liste associative factorisée)
						 	 traitant d'un coup le cas None et le cas Some :
						 	 on parcourt la bibliothèque ; cas de base : rien :
						 	 on ajoute une entrée pour le but avec ce seul raffinement ;
						 	 sinon : on traite entrée par entrée, jusqu'à trouver le but.
								Évite un double parcours de la bibliothèque !
						 	 **)
						 	 (** match ... **) current_library
					 	end
				*)
			end
		| _ -> raise (Bad_library_exception (standard_error_message "library_of_strings" (bad_library_error_message _CONST_rule_delimiter)))
	in
	library_of_strings_aux library_input []
;;


(** /!\ TRAITEMENT DU FICHIER D'ENTRÉE : À METTRE PLUTÔT EN PREMIER ? **)
(** /!\ Ici ou dans "synthese.ml" : ajouter une détection des dépendances ? **)
(* let rec formulae_of_file (filename: string): formula list = *)
let input_of_file (filename: string): trace * library =
	let file_strings = read_file filename in
	let input_trace = trace_of_string (List.hd file_strings)
	and input_library = library_of_strings (List.tl file_strings)
	in
	(input_trace, input_library)
;;

let input_of_file_with_data (filename: string): string * trace * library =
	match read_file filename with
	| instance_data::trace_string::library_strings ->
		(instance_data, trace_of_string trace_string, library_of_strings library_strings)
	| _ -> failwith ("input_of_file_with_data: badly formatted file " ^ filename)
;;

(******** END: INPUT ********)



(** /!\ Enlever espaces ?
Utiles seulement pour jeter un œil au fichier JSON manuellement. **)
(* creating the JSON string for a trace using its length *)
(** /!\ ATTENTION : numéroter plutôt à partir de 1 ???
	Dépend du choix pour l'annotation de l'arbre, à voir **)
let json_of_trace_length (trace_length: int): string =
	if trace_length = 0 then ""
	else
	begin
		let current_string = ref "0" in
		for index = 1 to trace_length - 1 do
			current_string := !current_string ^ ", " ^ (string_of_int index);
		done;
		!current_string
	end
;;


(* converting a list to a string, with appropriate commas
	and line breaks, using a string converter for the elements *)
let string_of_list (converter: 'a -> string) (_list: 'a list): string =
	let rec aux (converter: 'a -> string) (_list: 'a list) (start: bool): string =
	match _list with
	| [] -> (if start then "[" else "") ^ "]"
	| element::rest -> (if start then "[" else ",\n") ^ (converter element) ^ (aux converter rest false)
	in
	aux converter _list true
;;



(*** /!\ AJOUTER LE NOM DU BUT NON TERMINAL EN ANNOTATION JSON ? ***)
(** /!\ Enlever retours à la ligne ("\n") et espaces ?
Utiles seulement pour jeter un œil au fichier JSON manuellement. **)
let rec json_of_tree (_tree: tree): string =
	"{"
	^
	begin
		match _tree with

		| TERMINAL_tree (_terminal_goal, _annotation) ->
			let (_goal, start_index, end_index) = _annotation in
			let start_string = string_of_int start_index
			and end_string = string_of_int end_index in
			  "goal: \"" ^ start_string ^ "->" ^ end_string ^ "\",\n"
			  ^ "type: \"terminal\",\n"
			  ^ "begin: " ^ start_string ^ ", end: " ^ end_string

		| OR_tree (children, _annotation) -> 
			let (_goal, start_index, end_index) = _annotation in
			let start_string = string_of_int start_index
			and end_string = string_of_int end_index in
			  "goal: \"" ^ _goal ^ "\",\n"
			  ^ "type: \"OR\",\n"
			  ^ "begin: " ^ start_string ^ ", end: " ^ end_string ^ ",\n"
			  ^ "children: " ^ (string_of_list json_of_tree children)

		| SAND_tree ((children_1, children_2), _annotation) -> 
			let (_goal, start_index, end_index) = _annotation in
			let start_string = string_of_int start_index
			and end_string = string_of_int end_index in
			  "goal: \"" ^ _goal ^ "\",\n"
			  ^ "type: \"SAND\",\n"
			  ^ "begin: " ^ start_string ^ ", end: " ^ end_string ^ ",\n"
			  ^ "children: " ^ (string_of_list json_of_tree [children_1; children_2])

		| AND_tree (children, _annotation) -> 
			let (_goal, start_index, end_index) = _annotation in
			let start_string = string_of_int start_index
			and end_string = string_of_int end_index in
			  "goal: \"" ^ _goal ^ "\",\n"
			  ^ "type: \"AND\",\n"
			  ^ "begin: " ^ start_string ^ ", end: " ^ end_string ^ ",\n"
			  ^ "children: " ^ (string_of_list json_of_tree children)
	end
	^
 	"}"
;;


(** /!\ Enlever retours à la ligne ("\n") et espaces ?
Utiles seulement pour jeter un œil au fichier JSON manuellement. **)
(* converting a trace and tree to the JSON format *)
let json_of_output (trace_length: int) (_tree: tree): string =
	"{\n"
	^ "trace: [" ^ (json_of_trace_length trace_length) ^ "],\n"
	^ "tree: " ^ (json_of_tree _tree) ^ "\n"
	^ "}"
;;



(* converting an internal atom to user-input format *)
let user_format_of_atom (_atom: atom): string =
	_atom
;;

(* converting an internal valuation to user-input format *)
let user_format_of_atom_set (_atom_set: atom_set): string =
	match _atom_set with
	| [] -> ""
	| [_atom] -> user_format_of_atom _atom
	| _atom::other_atoms -> List.fold_left (fun current_string _atom -> current_string ^ (Char.escaped _CONST_element_delimiter) ^ (user_format_of_atom _atom)) (user_format_of_atom _atom) other_atoms
;;


(* converting an internal trace to user-input format *)
let user_format_of_trace (_trace: trace): string =
	let trace_string = ref "" in
	Array.iteri
		(
		fun index _valuation ->
			begin
				if index <> 0
				then trace_string := !trace_string ^ Char.escaped _CONST_atom_set_delimiter ^ " " ^ user_format_of_atom_set _valuation
				else trace_string := !trace_string ^ (user_format_of_atom_set _valuation)
			end
		)
		_trace
	;
	!trace_string
;;



(* converting an external formula to user-input format *)
(* PRECONDITION: the external formula is atomic *)
(* NB: this function is partial, as it is intended for PIC-driven
	ATS instances only, where the only possible external formulae
	are atomic
*)
let user_format_of_formula (_external_formula: external_formula): string =
	match _external_formula with
	| Ext_Atom atom_name -> atom_name
;;

(* converting a list of subgoals to user-input format *)
let rec user_format_of_subgoals (subgoals: goal list): string =
	match subgoals with
	| [] -> ""
	| (NONTERMINAL_GOAL _subgoal)::other_subgoals -> " " ^ _subgoal ^ user_format_of_subgoals other_subgoals
;;

(* converting an internal refinement to user-input format *)
let user_format_of_refinement ((_operator, subgoals): refinement): string =
	match _operator with
	| TERMINAL_OP ->
		let [TERMINAL_GOAL (start_formula, end_formula)] = subgoals in
		user_format_of_formula start_formula ^ ", " ^ user_format_of_formula end_formula
	| OR_OP -> "OR" ^ user_format_of_subgoals subgoals
	| SAND_OP -> "SAND" ^ user_format_of_subgoals subgoals
	| AND_OP -> "AND" ^ user_format_of_subgoals subgoals
;;


(* converting an internal library entry to user-input format *)
let rec user_format_of_rules ((goal_name, refinements): nonterminal_goal * refinement list): string =
	match refinements with
	| [] -> ""
	| _refinement::other_refinements ->
		(goal_name ^ (Char.escaped _CONST_rule_delimiter) ^ " " ^ user_format_of_refinement _refinement)
		^ "\n"
		^ (user_format_of_rules (goal_name, other_refinements))
;;

(* converting an internal library to user-input format *)
let user_format_of_library (_library: library): string =
	List.fold_left
		(fun current_string rules -> current_string ^ user_format_of_rules rules)
		""
		_library
;;
