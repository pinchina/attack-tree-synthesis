(******************* SYNTHESIS ************************)

(* let _TOUISTCOMMAND = "./touist"; *)
let _TOUISTCOMMAND = "touist";

(** /!\ POUR LA GESTION DES FICHIERS :
	remplacer les appels à "cat" via "Sys.command" par des "open_out"
	(cf. "generer_exemples_arbres.ml" pour la syntaxe)
**)

(* Attack tree synthesis: for one word/trace through SAT *)

(* /!\ POUR LE MOMENT, SAND SUPPOSÉS BINAIRES ;
  SINON/ENSUITE, ÉCRIRE PROCÉDURE DE PRÉTRAITEMENT
  (BINARISATION DES SAND) PUIS DE POST-TRAITEMENT
  (DÉBINARISATION DES SAND)
*)

(* /!\ PASSER LES BIBLIOTHÈQUE ET TRACE EN VARIABLES GLOBALES ?
  (Plutôt qu'en paramètres d'appel de fonctions.)
  Notamment pour les fonctions right_hand_formula et formula_of_rule. *)

(* /!\ ÉCRIRE UN PRÉ-TRAITEMENT POUR ÉLAGUER LA BIBLIOTHÈQUE
	DE SES BUTS NON PRODUCTIFS ?
	POUR LE MOMENT, BIEN VÉRIFIER QU'UNE ERREUR EST GÉNÉRÉE
	SI L'ON ESSAIE DE CONSTRUIRE UN ARBRE N'ABOUTISSANT PAS
*)


(******************* MODULES ************************)
open Types
open Exceptions
open Utilities
open Formulae
open Parsing
(******************* END MODULES ************************)



(******************* GLOBAL VARIABLES ************************)
(* index for annotating arbitrarily developed nodes *)
let arbitrary_index = -1;;

(* specific run times measurement: one for each phase *)
let formula_time = ref 0.;; (* time for building the SAT instance *)
let solving_time = ref 0.;; (* time for solving the SAT instance *)
let guiding_time = ref 0.;; (* time for building the tree *)
(******************* END GLOBAL VARIABLES ************************)


(* /!\ EN COURS *)
(* let variables_and_formulae_of_rule (rule: refinement_rule) (trace_length: int): (atom list * formula list) = match rule with
	| 
;; *)

(* /!\ EN COURS *)
(* let generate_SAT (rule_library: library): (atom list * formula list) =
	List.map f alist 
;; *)

(** INUTILE ?
(* ancillary function: apply another function for the first element
	that satisfies a predicate, if any
*)
let handle_first_match (condition: 'a -> bool) (_list: 'a list) (_function: 'a -> 'b) (special_treatment: 'a -> 'b): 'b list =
	let rec handle_first_match_aux (arbitrary: bool) (condition: 'a -> bool) (_list: 'a list) (_function: 'a -> 'b) (special_treatment: 'a -> 'b): 'b list =
		match _list with
		| [] -> []
		| element::rest ->
			if arbitrary then (_function element)::(handle_first_match_aux true condition rest _function special_treatment)
			else if (condition element)	then (special_treatment element)::(handle_first_match_aux true condition rest _function special_treatment)
			else (_function element)::(handle_first_match_aux false condition rest _function special_treatment)
	in
		handle_first_match_aux false condition _list _function special_treatment
;;
**)

(* returns some rule associated with a given goal in a library *)
(* implementation choice: the first rule *)
(* ancillary function for the arbitrary development of a tree in "valuation_to_attack_tree" *)
(* NB: we arbitrarily take the first rule to appear in the library;
	this choice could be tinkered with in order to obtain a different tree.
	/!\ POSSIBLE FUTURE CHANGE HERE.
		-> In such a case: maybe not the whole library as a paramater,
		but rather a mere library ENTRY (goal + its refinements)?
*)
let any_rule_of_goal (_goal: nonterminal_goal) (_named_library: named_library): refinement_rule =
	(_goal, fst (List.hd (List.assoc _goal _named_library)))

(** /!\ MOINS CONCIS **)
(* 	match _library with
	| [] -> failwith "any_rule_of_goal: goal " ^ _goal ^ " not found in the library"
	| (current_goal, refinements)::rest_of_library ->
		if String.equal _goal current_goal then
			match refinements with
			| [] -> failwith "any_rule_of_goal: goal " ^ _goal ^ " has no refinement"
			| first_refinement::_ -> (_goal, first_refinement)
		else any_rule_of_goal _goal rest_of_library
 *);;


(* ancillary function for "valuation_to_attack_tree":
	checks whether some goal has a refinement that matches the given valuation
	for a specified interval
*)
let rec find_suitable_rule (_goal, refinements: nonterminal_goal * named_refinement list) (start_index: int) (end_index: int) (input_valuation: valuation) (_named_library: named_library): refinement_rule option = 
	match refinements with
	| [] -> None
	| _named_refinement::other_refinements -> 
		(** if the associated variable is set to true, take the rule,
		otherwise, continue **)
		let rule = (_goal, fst _named_refinement) in
		let variable_string = format_of_formula _named_library (Atom (REFINEMENT_VAR (rule, start_index, end_index))) in
		if List.assoc variable_string input_valuation
		then Some rule
		else find_suitable_rule (_goal, other_refinements) start_index end_index input_valuation _named_library
;;



(* ancillary function for "find_SAND_subrules":
	finds a SAND junction between two subrules if any
*)
(* PRECONDITION: start_valuation and end_valuation
	contain only variables set to true and with suitable starting end ending
	indices respectively
*)
(* NB: possible future optimisation: when creating the library, sort refinements
	by ascending lexicographical order on (start index, end index), so that parsing
	for matching indices can be optimised (by managing two iterators/indices,
	somewhat as in quicksort)
*)
(** /!\ en cours :
	AJOUTER : vérification que les variables choisies figurent
	dans les valuations d'entrée **)
let rec find_junction (start_valuation: valuation) (end_valuation: valuation) (_named_library: named_library): int option =
	match (start_valuation, end_valuation) with
	| (_, []) -> None
	| ([], _::other_ends) -> find_junction start_valuation other_ends _named_library
	| ((start_variable_name, _)::other_starts, (end_variable_name, _)::other_ends) ->
		begin
			match variable_of_format start_variable_name _named_library with
			| REFINEMENT_VAR (_, _, index) ->
				if List.mem_assoc start_variable_name start_valuation
				then
					begin
						match variable_of_format end_variable_name _named_library with
						| REFINEMENT_VAR (_, other_index, _) ->
							if index = other_index && List.mem_assoc end_variable_name end_valuation
							then Some index
							else find_junction other_starts end_valuation _named_library
						| _ -> find_junction start_valuation other_ends _named_library
					end
				else find_junction other_starts end_valuation _named_library
			| _ -> find_junction other_starts end_valuation _named_library
		end
;;


(* ancillary function for "find_SAND_subrules":
	filter checking whether an assignement has a variable
	that is of type refinement and set to true
*)
let true_refinement (_named_library: named_library) ((variable_name, value): assignment): bool =
	value
	&&
	(
		match variable_of_format variable_name _named_library with
		| REFINEMENT_VAR (_, _, _) -> true
		| _ -> false
	)
;;

(* ancillary function for "find_SAND_subrules":
	checks whether an assignement has a variable
	that has a certain starting index
*)
(**/!\ VÉRIFIER QUE compare SE COMPORTE CORRECTEMENT
pour comparer des règles !! **)
let start_filter (start_index: int) (start_rule: refinement_rule) (_named_library: named_library) ((variable_name, _): assignment): bool =
	match variable_of_format variable_name _named_library with
	| REFINEMENT_VAR (rule, index_1, _) -> (**/!\SUPPRIMER**) if index_1 = 0 then print_endline ("Cette variable : " ^ variable_name ^ " ; val. de comparaison : " ^ string_of_int (compare rule start_rule));
	index_1 = start_index && compare rule start_rule = 0
	| _ -> false
;;


(* ancillary function for "find_SAND_subrules":
	checks whether an assignement has a variable
	that has a certain ending index
*)
(**/!\ VÉRIFIER QUE compare SE COMPORTE CORRECTEMENT
pour comparer des règles !! **)
let end_filter (end_index: int) (end_rule: refinement_rule) (_named_library: named_library) ((variable_name, _): assignment): bool =
	match variable_of_format variable_name _named_library with
	| REFINEMENT_VAR (rule, _, index_2) -> index_2 = end_index && compare rule end_rule = 0
	| _ -> false
;;


(* ancillary function for "valuation_to_attack_tree":
	choosing the two subrules for a binary SAND-node
	and the junction index
*)
(* Possible optimisation: instead of filtering the valuation at the start,
	build one with only true variables step by step, simultaneously with
	the parsing
*)
let find_SAND_subrules (input_valuation: valuation) (rule: refinement_rule) (start_index: int) (end_index: int) (_named_library: named_library): refinement_rule * refinement_rule * int =
	let (main_goal, (SAND_OP, [NONTERMINAL_GOAL subgoal_1; NONTERMINAL_GOAL subgoal_2])) = rule in
	let refinements_1 = List.map fst (List.assoc subgoal_1 _named_library)
	and refinements_2 = List.map fst (List.assoc subgoal_2 _named_library)
	(* retain only refinement variables set to true *)
	and pruned_valuation = List.filter (true_refinement _named_library) input_valuation
	in
		(**/!\SUPPRIMER**) print_endline "Raffinements candidats :"; List.iter (fun (var, value) -> print_endline (var ^ " " ^ string_of_bool value)) pruned_valuation ;
	let rec find_SAND_subrules_aux (refinements_1: refinement list) (refinements_2: refinement list): refinement_rule * refinement_rule * int =
		(**/!\SUPPRIMER**) print_endline "Raffinements 1 :"; List.iter print_refinement refinements_1;
		(**/!\SUPPRIMER**) print_endline "Raffinements 2 :"; List.iter print_refinement refinements_2;
		match (refinements_1, refinements_2) with
		| (_, []) -> failwith "find_SAND_subrules: could not find matching subrules" (* should not happen *)
		| ([], refinement_2::other_refinements_2) -> find_SAND_subrules_aux refinements_1 other_refinements_2
		| (refinement_1::other_refinements_1, refinement_2::other_refinements_2) ->
			let subrule_1 = (subgoal_1, refinement_1)
			and subrule_2 = (subgoal_2, refinement_2)
			in
			(**/!\SUPPRIMER**) print_endline "Sous-règles 1 et 2 :"; print_rule subrule_1; print_rule subrule_2;
			(** /!\ beware the numbering convention for indices: starting from 0 **)
			let start_valuation = List.filter (start_filter start_index subrule_1 _named_library) pruned_valuation
			and end_valuation = List.filter (end_filter end_index subrule_2 _named_library) pruned_valuation
			in
			(**/!\SUPPRIMER**) print_endline ("déb. : " ^ string_of_int start_index ^ " ; fin : " ^ string_of_int end_index) ;
			(**/!\SUPPRIMER**) print_endline "Valuation élaguée :" ;
			(**/!\SUPPRIMER**) List.iter (fun (var, value) -> print_endline (var ^ " " ^ string_of_bool value)) pruned_valuation ;
			(**/!\SUPPRIMER**) print_endline "Valuation départ :" ;
			(**/!\SUPPRIMER**) List.iter (fun (var, value) -> print_endline (var ^ " " ^ string_of_bool value)) start_valuation ;
			(**/!\SUPPRIMER**) print_endline "Valuation arrivée :" ;
			(**/!\SUPPRIMER**) List.iter (fun (var, value) -> print_endline (var ^ " " ^ string_of_bool value)) end_valuation ;
			match find_junction start_valuation end_valuation _named_library with
			| None -> (**/!\SUPPRIMER**) print_endline "Non joignables.";
			find_SAND_subrules_aux other_refinements_1 refinements_2
			| Some intermediate_index -> (**/!\SUPPRIMER**) print_endline ("Val. intermédiaire : " ^ string_of_int intermediate_index);
			(subrule_1, subrule_2, intermediate_index)
	in
	find_SAND_subrules_aux refinements_1 refinements_2
;;



(* ancillary function for "valuation_to_attack_tree":
	finds a cover, for an AND-node
*)
let find_cover (input_valuation: valuation) (rule: refinement_rule) (start_index: int) (end_index: int) (_named_library: named_library): interval_set list =

	let (main_goal, (AND_OP, subgoals)) = rule in
	match subgoals with
	| [] -> []
	| (NONTERMINAL_GOAL subgoal)::other_subgoals ->

		(* finds a cover with subrule indices *)
		let rec find_cover_aux (input_valuation: valuation): (interval_set * int) list = match input_valuation with
			| [] -> []
			| (_, false)::other_assignments -> find_cover_aux other_assignments
			| (variable_string, true)::other_assignments ->
				match variable_of_format variable_string _named_library with
					| INTERVAL_VAR (variable_rule, variable_start_index, variable_end_index, which_subrule, substart_index, subend_index) ->
						if (compare rule variable_rule = 0 && variable_start_index = start_index && variable_end_index = end_index)
						then ((substart_index, subend_index), which_subrule)::find_cover_aux other_assignments
						else find_cover_aux other_assignments
					| _ -> find_cover_aux other_assignments
		in

		(* sort subrules according to their indices *)
		let sorted_indiced_cover = List.sort (fun (_, index1) (_, index2) -> compare index1 index2) (find_cover_aux input_valuation)
		in List.map fst sorted_indiced_cover
;;


(* choosing the subrules (R_k) for an AND-node *)
let rec find_AND_subrules (cover_with_goals: (interval_set * goal) list) (input_valuation: valuation) (_named_library: named_library): refinement_rule list =
	match cover_with_goals with
	| [] -> []
	| ((start_index, end_index), NONTERMINAL_GOAL _goal):: other_intervals ->
		let refinements = List.assoc _goal _named_library in
		match find_suitable_rule (_goal, refinements) start_index end_index input_valuation _named_library with
		| Some rule -> rule::(find_AND_subrules other_intervals input_valuation _named_library)
		| None -> failwith "find_AND_subrules: no suitable subrule found" (* should not happen *)
;;


(** /!\ utiliser une bibliothèque nommée ou anonyme ? **)
(** /!\ ATTENTION : plutôt nommer la bibliothèque une fois pour toutes
  AVANT de la passer en paramètre à "valuation_to_attack_tree" ;
  risque de problème, sinon ??
**)
(* tries to find an attack tree associated with a given valuation *)
(* "arbitrary" is a mode used to extract a tree without looking at the valuation *)
(*** /!\ PRENDRE EN COMPTE le mode arbitaire pour les nœuds SAND et AND ! ***)
let rec valuation_to_attack_tree ?(arbitrary: bool = false) (_named_library: named_library) (input_valuation: valuation) (rule: refinement_rule) (start_index: int) (end_index: int): tree =
	let main_annotation = ((fst rule: nonterminal_goal), start_index, end_index)
	in
 	match (snd rule: refinement) with
	| (TERMINAL_OP, [TERMINAL_GOAL _terminal_goal]) -> TERMINAL_tree (_terminal_goal, main_annotation)

	| (OR_OP, subgoals) ->

		(* in arbitrary mode, all nodes are developed
		independently of the valuation *)
		let rec list_of_subtrees ?(arbitrary: bool = false) (subgoals: goal list) =
			match subgoals with
			| [] -> []
			| (NONTERMINAL_GOAL subgoal)::other_subgoals ->
				if arbitrary then
					let subroot_rule = any_rule_of_goal subgoal _named_library in
					(valuation_to_attack_tree ~arbitrary:true _named_library input_valuation subroot_rule arbitrary_index arbitrary_index)
					::(list_of_subtrees ~arbitrary:true other_subgoals)
				else
					match find_suitable_rule (subgoal, List.assoc subgoal _named_library) start_index end_index input_valuation _named_library with
					| None -> 
						let subroot_rule = any_rule_of_goal subgoal _named_library in
						(valuation_to_attack_tree ~arbitrary:false _named_library input_valuation subroot_rule arbitrary_index arbitrary_index)
						::(list_of_subtrees other_subgoals)
					| Some subroot_rule -> 
						(valuation_to_attack_tree _named_library input_valuation subroot_rule start_index end_index)
						::(list_of_subtrees ~arbitrary:true other_subgoals)
		in
		let subtree_list = list_of_subtrees subgoals in 
			OR_tree (subtree_list, main_annotation)

	(**** /!\ Moins pratique ?
		if arbitrary then
			let tree_list = List.map (fun subgoal -> valuation_to_attack_tree ~arbitrary:true input_library input_valuation (any_rule_of_goal subgoal) start_index end_index) subgoals
			in
			OR_tree (tree_list, main_annotation)
		else
		(** /!\ utiliser la fonction auxiliaire "handle_first_match"
		qui change de comportement une fois le premier élément souhaité trouvé :
		passer en mode arbitraire une fois la première variable à vrai trouvée **)
		(** /!\ écrire ; utiliser le mode "arbitrary" (à "true")
		pour gérer les sous-buts à développer de manière arbitraire **)
		let tree_list =
			handle_first_match
				(** /!\ VALUATION de la bonne variable **)
				_library
				(** /!\ valuation_to_attack_tree en arbitraire **) (any_rule_of_goal (** /!\ PRÉCISER POUR PARAMÈTRES **) _library)
				(** /!\ valuation_to_attack_tree en non arbitraire **) (** /!\ En trouvant bonne règle par rapport à la valuation **)
		in
		OR_tree (tree_list, main_annotation)
	****)


	(** /!\ À COMPLÉTER : cas de raffinement SAND ;
	répercuter l'algorithme valuationToAttackTree **)

	(** /!\ EN COURS **)
	(** /!\ N.B. : pas besoin pour les instances PIC,
	mais besoin pour l'exemple du musée. **)
	 	
	| (SAND_OP, subgoals) ->
		(** /!\ vérifier, mais semble correct **)
		(**/!\SUPPRIMER**) print_endline ("index déb. : " ^ string_of_int start_index ^ " ; index fin : " ^ string_of_int end_index);
		let (subrule_1, subrule_2, intermediate_index) = find_SAND_subrules input_valuation rule start_index end_index _named_library
		in
		(**/!\SUPPRIMER**) print_endline ("index inter. : " ^ string_of_int intermediate_index);
		let tree_1 = valuation_to_attack_tree _named_library input_valuation subrule_1 start_index intermediate_index
		and tree_2 = valuation_to_attack_tree _named_library input_valuation subrule_2 intermediate_index end_index
		in
		SAND_tree ((tree_1, tree_2), main_annotation)

	| (AND_OP, subgoals) ->
		(* we select a cover with associated subrules *)
		let interval_list = find_cover input_valuation rule start_index end_index _named_library
		in
		(** /!\ TRY pour débuggage, à enlever ensuite **)
		try
			let subrule_list = find_AND_subrules (List.combine interval_list subgoals) input_valuation _named_library
			in
			let tree_list = List.map2 (fun subrule interval -> valuation_to_attack_tree _named_library input_valuation subrule (fst interval) (snd interval)) subrule_list interval_list
			in
			AND_tree (tree_list, main_annotation)
		with
		| Invalid_argument "List.combine" ->
			(
				print_refinement (AND_OP, subgoals);
				print_int (List.length interval_list);
				print_int (List.length subgoals);
				exit 2;
			)
;;


(* finding a suitable root rule among a valuation *)
let rec find_root_rule (_valuation: valuation) (trace_length: int) (_named_library: named_library): refinement_rule =
	match _valuation with
	| [] ->  raise No_tree_exception
	| (_, false)::other_assignments -> find_root_rule other_assignments trace_length _named_library
	| (variable_name, true)::other_assignments ->
		begin
			match variable_of_format variable_name _named_library with
			| REFINEMENT_VAR (rule, 0, end_index) ->
				if end_index = (trace_length - 1) then rule
				else find_root_rule other_assignments trace_length _named_library
			| _ -> find_root_rule other_assignments trace_length _named_library
		end
;;


(** /!\ N.B. : les appels système utilisent une syntaxe Linux ;
	à signaler dans le README ?
	Voire permettre l'exécution sur d'autres systèmes d'exploitation,
	via sa détection par "Sys.os_type".
**)
(* synthesis function, solving the main problem *)
(** /!\ À FAIRE : produire un ensemble de formules (entrée du problème SAT) ;
	notamment, produire les formules pour les variables d'intervalle. **)
(** /!\ CHOIX À EFFECTUER : demander des règles nommées ou non nommées ? **)
(** /!\ Pour alléger : plutôt passer input_library comme variable globale ? **)
let synthesize (input_trace: trace) (input_library: library): tree =

	let trace_length = Array.length input_trace
	and named_input_library = name_library input_library
	in

	(* /!\ À FAIRE (un peu plus haut / en amont ?) : vérifier que la bibliothèque
		est correcte, notamment via correct_refinement
		OU inutile ? *)

	(* /!\ ÉVENTUELLEMENT : binarisation des SAND ; pour plus tard *)

	(* we start measuring SAT-building time here *)
	formula_time := Sys.time ();

	(** propagation formulae **)
	let input_formulae = ref [] in
	let rec generate_propagation_formulae (_library: library): unit = 
		match _library with
		| [] -> ()
		| (_goal, refinements)::other_rules ->
			begin
				for start_index = 0 to (trace_length - 1) do
					for end_index = start_index to (trace_length - 1) do
						let add_goal_rules _refinement =
							(
								(* NB: we use "input_library" rather than "_library"
								because we need the full information on the library,
								not merely the entries that are yet to be parsed *)
								input_formulae := (formula_of_rule (_goal, _refinement) start_index end_index input_library input_trace)::!input_formulae
							)
						in List.iter add_goal_rules refinements
					done
				done;
				generate_propagation_formulae other_rules
			end
	in
	generate_propagation_formulae input_library;
	
	(* appending the constraint of having a root for the tree *)
	let rec root_atoms (_library: library): formula list =
		match _library with
		| [] -> []
		| (_goal, refinements)::other_entries -> 
			begin
				match refinements with
				| [] -> root_atoms other_entries
				| _refinement::other_refinements ->
					(** /!\ VÉRIFIER pour les indices : 0, n-1 OU 1,n **)
					(Atom (REFINEMENT_VAR ((_goal, _refinement), 0, trace_length - 1)))
					::(root_atoms ((_goal, other_refinements)::other_entries))
			end
	in let root_formula = list_to_disjunction (root_atoms input_library)
	in input_formulae := root_formula::!input_formulae;

	(** /!\ TEST **) (* print_formula root_formula named_input_library; *)

	let formatted_list = List.map (format_of_formula named_input_library) !input_formulae 
	and (* input_file = Filename.temp_file "input_file_" "" in *)
		(input_file, write_channel) = Filename.open_temp_file "input_file_" "" in
	output_string write_channel (string_of_lines formatted_list);

	close_out write_channel;

	(* we compute SAT-building time here *)
	(** NB: we check duration AFTER the creation of the input file,
	as this is a bottleneck in terms of execution time,
	because of system calls **)
	formula_time := Sys.time () -. !formula_time;

	(* /!\ SUPPRIMER le fichier témoin (entrée) ;
	ou le laisser à titre expérimental voire après ??
	Ou permettre suppression en option ? *)
	(* store input in a file, as a record *)
	(*
	let write_channel_test = open_out "input_file" in
	output_string write_channel_test (string_of_lines formatted_list);
	*)
	(* Sys.command "rm input_file"; *) (* /!\ À permettre en option ? *)

	(*** /!\ Encapsuler toute la procédure d'appel à Touist et de traduction du résultat ?
		Pour pouvoir adapter selon le solveur utilisé.
	***)

	(* we start measuring solving time *)
	(** NB: we start measuring BEFORE the creation of the input file,
	as this system call is not negligible **)
	solving_time := Sys.time ();

	(* we create a fresh temporary file with randomised name *)
	let valuation_file_name = Filename.temp_file "valuation_file_" "" in

	(* external call to Touist, which returns a (temporary) valuation file,
	possibly empty *)
	(* Sys.command (_TOUISTCOMMAND ^ " --solve touist_test > " ^ valuation_file_name); *) (** /!\ VERSION DE TEST **)

	Sys.command (_TOUISTCOMMAND ^ " --solve " ^ input_file ^ " > " ^ valuation_file_name); (** /!\ VERSION RÉELLE **)

	(**** /!\ TEST EN COURS  : affichage des formules *****)
	(*
		close_out write_channel_test;
		Sys.command (_TOUISTCOMMAND ^ " --solve " ^ "input_file" ^ " > " ^ "valuation_file"); (** /!\ VERSION RÉELLE **)
	*)
	(**** /!\ FIN : TEST EN COURS *****)

	(* parsing the valuation file: we get a list of strings *)
	let valuation_lines = read_file valuation_file_name in

	(* /!\ SUPPRIMER le fichier témoin (résultat) ? *)
	(* store resulting valuation in a file, as a record *)
	(* Sys.command ("cat " ^ valuation_file_name ^ " > valuation_file"); *)
	(* Sys.command "rm valuation_file"; *) (* /!\ À permettre en option ? *)

	(* we compute solving time here *)
	solving_time := Sys.time () -. !solving_time;

	(* handle the resulting file to translate the valuation we get *)	
	match valuation_lines with

	(* if there is no valuation, Touist returns an empty file *)
	| [] ->
		begin
			guiding_time := 0.;
			raise No_tree_exception
		end

	| _ ->

		let (_valuation: valuation) = valuation_of_strings valuation_lines in

		(* /!\ ÉVENTUELLEMENT : débinarisation des SAND ; pour plus tard *)

		(* we start measuring guiding time *)
		guiding_time := Sys.time ();

		try
			(** FIRST STEP: find a variable assigned true
			for the total interval [1, n (= trace_length)]. **)
			let root_rule = find_root_rule _valuation trace_length named_input_library in

			(** SECOND STEP: a tree is then developed starting from the previously
			selected rule **)
			let result_tree = valuation_to_attack_tree named_input_library _valuation root_rule 0 (trace_length - 1) in

			(**
			print_endline ("Result tree:"); (* /!\ À ENLEVER ? *)
			print_endline (json_of_tree result_tree); (* /!\ À ENLEVER ? OU/ET écrire dans un fichier ? *)
			**)
			(* we compute guiding time here *)
			guiding_time := Sys.time () -. !guiding_time;
			result_tree
		with
			No_tree_exception ->
				begin
					(* we compute guiding time here, but this should not happen *)
					guiding_time := Sys.time () -. !guiding_time;
					raise No_tree_exception
				end
;;

(*** /!\ À DÉCOMMENTER : lancement de la synthèse ***)
(***
(** /!\ À FAIRE : demander interactivement les arguments ?
	Ou dans l'interface finale ?
	Ou/Et lancer via un fichier utilisateur ?
**)
(* synthesis using user's input *)
(** /!\ UTILISER le fichier d'entrée ; à demander en SEUL argument
	OU utiliser deux fichiers : un de trace et un de bibliothèque ??
**)
let input_trace = Sys.argv.(1) and input_library = Sys.argv.(2) in
	(* /!\ À FAIRE : traduire les arguments (qui sont des CHAÎNES) en le bon type *)
(* synthesize input_trace input_library;; *)
try
	let test_library = [("g", [(OR_OP, [])])] in (** /!\ POUR TEST ; à enlever ensuite **)
	(* (nonterminal_goal * refinement list) list *)
	synthesize [|["p"]|] test_library
with
(** /!\ OU étendre le type arbre et renvoyer un arbre vide ? **)
| No_tree_exception -> failwith "The trace cannot be explained through the library."
***)
