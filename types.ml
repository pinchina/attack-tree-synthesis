(******************* TYPES ************************)

(* proposition atom for the trace alphabet *)
type atom = string

(* subset of atomic proposition *)
(* /!\ AUTORISER LES DOUBLONS D'ATOMES DANS LES ENSEMBLES OU PAS ?? *)
type atom_set = atom list

(* a trace is an array of sets of atomic propositions *)
type trace = atom_set array

(* Non-terminal goal g *)
type nonterminal_goal = string

(* external formulae used to express configuration properties *)
(* NB: not to be confused with internal SAT formulae *)
(* NB: in this implementation, expressed under Touist format *)
type external_formula =
| Ext_True
| Ext_False
| Ext_Atom of atom
| Ext_Not of external_formula
| Ext_And of external_formula * external_formula
| Ext_Or of external_formula * external_formula
| Ext_Implies of external_formula * external_formula


(* In order to well separate the goal's formal name from its interpration as an ordered pair <iota, gamma> *)
type terminal_goal = external_formula * external_formula

type goal =
| NONTERMINAL_GOAL of nonterminal_goal
| TERMINAL_GOAL of terminal_goal

(* each node is annotated with non-terminal goals
and indices	of the matching trace interval *)
type annotation = nonterminal_goal * int * int

(* annotated attack tree *)
type tree =
| TERMINAL_tree of terminal_goal * annotation
| OR_tree of (* (tree * tree) *) tree list * annotation (* /!\ OU binarisé ? *)
| SAND_tree of (tree * tree) * annotation (* /!\ OU liste ? Lié au traitement de la version binarisée, donc attention *)
| AND_tree of tree list * annotation

(* attack tree operators *)
type operator = TERMINAL_OP | OR_OP | SAND_OP | AND_OP

(* NB: refinement rules are presumed to be flattened: no nested connectors *)
(* NB: for a terminal refinement, there is exactly one (terminal) subgoal *)
type refinement = operator * (goal list)

(* refinement with associated rule name *)
type named_refinement = refinement * string


(* refinement rule *)
(* NB: HYPOTHESIS: flattened rules (no nesting of connectors) *)
type refinement_rule = nonterminal_goal * refinement

(* refinement library *)
(* representation: each ordered pair: (goal g, list of all refinements for g) *)
(* NB: HYPOTHESIS: no cycles in the goal dependency graph: well-founded order *)
(* type library = refinement_rule list *)
type library = (nonterminal_goal * refinement list) list

(* with a rule name in addition each time *)
type named_library = (nonterminal_goal * named_refinement list) list

(* x^R_(i, j): Boolean variable with rule and indexing *)
type refinement_variable = refinement_rule * int * int

(* x^g_(i, j): variable before instantiating with (several) R(g), as a disjunction *)
(* technical: so as to instantiate more easily with all the rules matching a goal *)
type goal_variable = nonterminal_goal * int * int

(* y^(R, i, j)_(k, i', j'): Boolean interval variable with rule and indexing:
	(i, j) are the bounds of the surrounding interval,
	while (i', j') are those targetted locally *)
type interval_variable = refinement_rule * int * int * int * int * int

type variable =
| REFINEMENT_VAR of refinement_variable
| INTERVAL_VAR of interval_variable
| GOAL_VAR of goal_variable
(* /!\ OU UN TABLEAU GLOBAL ?? Ou les deux, le tableau servant d'intermédiaire / de mémoire globale (= la valuation) ? *)

(* internal formulae for the SAT instance *)
(* NB: not to be confused with external configuration formulae *)
type formula =
| True (** NB: especially for handling a special case in function "uncovered_conjunction" **)
| False
| Atom of variable
| Not of formula
| And of formula * formula
| Or of formula * formula
| Implies of formula * formula



(** /!\ INUTILE ? **)
(*****
(* Refinement atoms x^R_(i, j) from the SAT instance: global association list:
  R -> (i, j) *)
type sat_refinement_atoms = (refinement_rule * (int * int) array) list

(* Interval atoms y^R_(k, i, j) from the SAT instance: global association list:
  (R, k) -> (i, j) *)
type sat_interval_atoms = ((refinement_rule * int) * (int * int) array) list

(* SAT instance atoms: global *)
(*
type sat_atoms =
| SAT_REFINEMENT_ATOMS of sat_refinement_atoms
| SAT_INTERVAL_ATOMS of sat_interval_atoms
*)
type sat_atoms = variable list
*****)



(* an assignment of a Boolean variable is a name and a Boolean value *)
type assignment = string * bool

(* a valuation associates a Boolean to each variable *)
(* NB: an association list is more practical than a function,
	for modifications (appending) *)
(* type valuation = (variable * bool) list *)
type valuation = assignment list

(* an interval is defined by its bounds: left bound, right bound *)
type interval_set = int * int

(* a pack is a set (= list) of intervals *)
type pack = interval_set list

(* PIC problem instance:
	an integer N (= target interval size)
	and a finite number of packs (= integer interval sets) *)
type pic_instance = int * pack list
