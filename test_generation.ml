(******************* TEST_GENERATION ************************)

(** This module aims at generating a benchmark of problem instances
	using the PIC problem. Each file is made up of the size of its
	instance as its first line, followed by a standard user input.
	REMINDER: "PIC" = "Packed Interval Problem").
**)

(** /!\ Ou générer les instances ATS directement, sans passer par la forme PIC ?
	Si oui, faire attention à l'indice i dans les buts terminaux <p_i, p_j> :
	on doit avoir 0 <= i < j <= N.
**)

open Types
open Parsing


(******** GLOBAL VARIABLES ********)

let _PIC_terminal_goal_string: string = "gTERM";;
let _PIC_OR_goal_string: string = "gOR";;
let _PIC_AND_goal_string: string = "gAND";;

(* in order to evaluate the size of the instance and file it accordingly *)
let library_size = ref 0;;

(* directory for storing the generated instances *)
let _DIRECTORY = "benchmark/";;

(* global generation parameters *)
let min_trace_length = ref 2;;
let max_trace_length = ref 2;;
let min_AND_arity = ref 1;;
let max_AND_arity = ref 1;;
let min_OR_arity_bound = ref 1;;
let max_OR_arity_bound = ref 1;;
let number_of_tests = ref 1;;


(******** END: GLOBAL VARIABLES ********)



(* generating a random set of packs
with N = interval_length,
M = number of packs
and max_pack_size = maximal pack size *)
let rec generate_packs (interval_length: int) (number_of_packs: int) (max_pack_size: int): pack list =

	let rec generate_pack (number_of_intervals: int): pack =
		match number_of_intervals with
		| 0 -> []
		| _ ->
			let interval_start = Random.int interval_length + 1 in
			let interval_end = interval_start + (Random.int (interval_length + 1 - interval_start)) in
			(interval_start, interval_end)::(generate_pack (number_of_intervals - 1))
	in
	begin
		match number_of_packs with
		| 0 -> []
		| _ -> 
			(* NB: at least one interval in any pack, hence "+ 1" *)
			let pack_size = (Random.int max_pack_size) + 1 in
			library_size := !library_size + 2 * pack_size;
			(generate_pack pack_size)::(generate_packs interval_length (number_of_packs - 1) max_pack_size)
	end
;;

(* generating a random PIC instance
with N = interval_length,
M = number of packs
and max_pack_size = maximal pack size *)
let generate_PIC (interval_length: int) (number_of_packs: int) (max_pack_size: int): pic_instance =
		(interval_length, generate_packs interval_length number_of_packs max_pack_size)
;;

(* converting a PIC interval into a refinement *)
let refinement_of_interval ((interval_start, interval_end): interval_set) (pack_number: int) (interval_number: int): refinement =
	let atom_start = string_of_int (interval_start - 1) (* NB: left drift *)
	and atom_end = string_of_int interval_end
	in
	(TERMINAL_OP, [TERMINAL_GOAL (Ext_Atom atom_start, Ext_Atom atom_end)])
;;


(** /!\ REMODIFIER ?? Pas couples, plutôt simple liste ? **)
(* converting a PIC pack to a list of pairs of subgoals:
	1) A non-terminal one for the OR-rule;
	2) One for the associated terminal rule.
*)
(* pack_number is used to avoid goal homonymy between subgoals stemming
	from different packs *)
let library_of_pack (_pack: pack) (pack_number: int): library * goal list =
	let rec library_of_pack_aux (_pack: pack) (pack_number: int) (interval_number: int): library * goal list =
	match _pack with
	| [] -> ([], [])
	| interval::rest_of_pack ->
		let (terminal_library, terminal_subgoals) = library_of_pack_aux rest_of_pack pack_number (interval_number + 1) (** /!\ OU -1 ? **)
		and goal_name =  (_PIC_terminal_goal_string ^ "_" ^ (string_of_int pack_number) ^ "_" ^ (string_of_int interval_number))
		in
		(
			(
				goal_name,
				[refinement_of_interval interval pack_number interval_number] (* refinement list *)
			)
			::terminal_library
			,
			(NONTERMINAL_GOAL goal_name)::terminal_subgoals
		)
	in
	let (terminal_library, terminal_subgoals) = library_of_pack_aux _pack pack_number 0
	and goal_name = (_PIC_OR_goal_string ^ "_" ^ (string_of_int pack_number))
	in
	(
		(goal_name, [(OR_OP, terminal_subgoals)])::terminal_library (* rule for OR *)
		,
		terminal_subgoals
	)
;;


(** /!\ In case there is a binarisation of ORs for complexity reasons,
binarise OR-rules **)
(* generating a library (with subgoals) associated to a list of packs *)
let library_of_packs (packs: pack list): library =
	let rec library_of_packs_aux (packs: pack list) (pack_number: int): library * goal list =
	match packs with
	| [] -> ([], [])
	| _pack::other_packs ->

	let (_ORlibrary, subgoals) = library_of_packs_aux other_packs (pack_number + 1)
	and (terminal_library, terminal_subgoals) = library_of_pack _pack pack_number in

 		(
 			terminal_library@_ORlibrary (** /!\ Optimisable ?? Car concaténation. **)
 			,
			(NONTERMINAL_GOAL (_PIC_OR_goal_string ^ "_" ^ (string_of_int pack_number)))::subgoals
 		)

	in
	let (_ORlibrary, subgoals) = library_of_packs_aux packs 0 (* pack numbering starts at zero *)
	in
	(_PIC_AND_goal_string, [(AND_OP, subgoals)])::_ORlibrary
;;


(* converting a PIC instance to an ATS (= tree synthesis) instance *)
let _ATS_of_PIC (interval_length, packs: pic_instance): trace * library =
	let _trace = Array.make (interval_length + 1) [""] in
	Array.iteri (fun index _ -> _trace.(index) <- [string_of_int index]) _trace;

	(_trace, library_of_packs packs)
;;


(*******************************************)
let generate_tests (manual: bool): unit =

	if manual then
		begin
			print_endline "Trace length (>= 2):";
			let interval_length = int_of_string (read_line ()) - 1 in
				min_trace_length := interval_length;
				max_trace_length := interval_length;
			print_endline "Number of children for AND:";
			let number_of_packs = int_of_string (read_line ()) in
				min_AND_arity := number_of_packs;
				max_AND_arity := number_of_packs;
			print_endline "Maximal number of leaves per OR:";
			let max_pack_size = int_of_string (read_line ()) in
				min_OR_arity_bound := max_pack_size;
				max_OR_arity_bound := max_pack_size;
			print_endline "How many tests?";
			number_of_tests := int_of_string (read_line ());
		end
	else
		();

	print_endline ("Enter file name prefix (files will be numbered and created in directory \"" ^ _DIRECTORY ^ "\"):");
	let file_name_prefix = read_line () in

	(** /!\ OU pas de tel aléatoire, au moins au début des expériences ? **)
	(* system-dependent randomisation *)
	Random.self_init ();

	for interval_length = !min_trace_length to !max_trace_length do
	for number_of_packs = !min_AND_arity to !max_AND_arity do
	for max_pack_size = !min_OR_arity_bound to !max_OR_arity_bound do
	for test_number = 1 to !number_of_tests do

		(* initializing the computation of the library size;
		incremented as a side effect of the generation *)
		library_size := 2 * number_of_packs + 1;

		(** generate a PIC instance **)
		let _PIC_input = generate_PIC interval_length number_of_packs max_pack_size
		in
		let (input_trace, input_library) = _ATS_of_PIC _PIC_input
		and _ATS_instance_size = !library_size + interval_length + 1
		in

		let result_string =
			(* The first line contains the data
			(size, trace length, AND arity, max OR arity)
			of the ATS instance. *)
			(string_of_int _ATS_instance_size)
			^ " " ^ (string_of_int interval_length)
			^ " " ^ (string_of_int number_of_packs)
			^ " " ^ (string_of_int max_pack_size)
			^ "\n"
			^ (user_format_of_trace input_trace)
			^ "\n"
			^ (user_format_of_library input_library)
		in
	
		(** write result in a numbered file **)
		let file_name =
			file_name_prefix
			^ "N" ^ string_of_int interval_length
			^ "M" ^ string_of_int number_of_packs
			^ "S" ^ string_of_int max_pack_size
			^ "_" ^ string_of_int test_number
		in
		let result_file = open_out (_DIRECTORY ^ file_name) in
		output_string result_file result_string;
		close_out result_file;

	done
	done
	done
	done
;;
(*******************************************)
